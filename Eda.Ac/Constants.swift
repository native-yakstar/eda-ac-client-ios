//
//  Constants.swift
//  Eda.Ac
//
//  Created by dingo on 23.11.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit

public let serverURL = "http://eda.ac/"
public let serverApiURL = serverURL + "api/v1/"
public let serverImageUrl = serverURL + "uploads/"
public let roubleSymbol = "₽"
public let mainColor = #colorLiteral(red: 0.6117647059, green: 0.8078431373, blue: 0.2941176471, alpha: 1)
public let redColor = #colorLiteral(red: 0.8428374529, green: 0.2652530372, blue: 0.3083832562, alpha: 1)
public var adminAccessToken = String()
public var legacyInfoURL = serverURL + "offer.html"

class LocalCache {
    
    var stores: [StoreInfo] {
        get {
            return self.stores
        }
        set {
            self.stores = newValue
        }
    }
    
    init() {
        print("Local cache started")
    }
    
}

public let ADMIN_ORDER_STATUSES = ["Неактивный заказ", "Активный", "Завершен", "Отмененен"]
