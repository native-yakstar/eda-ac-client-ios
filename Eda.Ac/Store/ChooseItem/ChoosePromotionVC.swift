//
//  ChoosePromotionVC.swift
//  Eda.Ac
//
//  Created by dingo on 05.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import YTBarButtonItemWithBadge

class ChoosePromotionVC : UITableViewController {
    
    //UI : CONTROLLERS
    @IBOutlet weak var promotionImage: UIImageView!
    @IBOutlet weak var promotionTitle: UILabel!
    @IBOutlet weak var promotionPrice: UILabel!
    @IBOutlet weak var promotionDesc: UILabel!
    
    @IBOutlet weak var toBasketBtn: UIButton!
    @IBOutlet weak var toBasketManageBtnsView: UIView!
    @IBOutlet weak var toBasketCountLabel: UILabel!
    
    
    let basketBtn = YTBarButtonItemWithBadge();

    
    var promotion = Product(.Promotion)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: mainColor]
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "arrow"), style: .plain, target: self, action: #selector(hideVC))
        navigationController?.navigationBar.tintColor = mainColor
        
        //Create basket badge
        basketBtn.setHandler(callback: openBasket);
        basketBtn.setImage(image: #imageLiteral(resourceName: "backet"));
        basketBtn.setBadge(value: BasketHandler().getCurrentBasketTotalCount());
        self.navigationItem.setRightBarButton(basketBtn.getBarButtonItem(), animated: true);
        
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.updateBasketUI), name: .basketChanged, object: nil)
        
        initView()
    }
    
    func initView(){
        if let url = URL(string: serverImageUrl + promotion.imageURL){
            promotionImage.af_setImage(withURL: url)
        }
        
        promotionTitle.text = promotion.title
        title = promotion.title
        promotionDesc.text = promotion.desc
        promotionPrice.text = "\(promotion.price) \(roubleSymbol)"
        
        toBasketBtn.layer.cornerRadius = toBasketBtn.layer.frame.height / 2
        
        toBasketManageBtnsView.layer.borderColor = mainColor.cgColor
        toBasketManageBtnsView.layer.borderWidth = 1.0
        toBasketManageBtnsView.layer.cornerRadius = toBasketManageBtnsView.layer.frame.height / 2
        toBasketManageBtnsView.layer.masksToBounds = true
        toBasketManageBtnsView.isHidden = true
        
        self.basketBtn.setBadge(value: BasketHandler().getCurrentBasketTotalCount());
        
        let count = BasketHandler().productCountInBasket(product_id: promotion.id)
        
        if count == 0 {
            //Его нету в корзине
            toBasketManageBtnsView.isHidden = true
            toBasketBtn.isHidden = false
        } else {
            //Он в корзине
            toBasketManageBtnsView.isHidden = false
            toBasketBtn.isHidden = true
            
            toBasketCountLabel.text = "\(count) шт. \(Float(count) * promotion.price) \(roubleSymbol)"
        }
    }
    
    @objc func hideVC() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductInPromotionCell") as! ProductInPromotionCell
        
        let productInPromotion = promotion.PromotionsProducts[indexPath.row]
        
        if let url = URL(string: serverImageUrl + productInPromotion.imageURL){
            cell.productImage.af_setImage(withURL: url)
        }
        cell.productDesc.text = productInPromotion.desc
        cell.productTitle.text = productInPromotion.title
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Products", bundle: nil)
        let productController = storyBoard.instantiateViewController(withIdentifier: "ChooseProductVC") as! ChooseProductVC
        
        var tmpProduct = Product.init(.Product)
        tmpProduct.id = promotion.PromotionsProducts[indexPath.row].id
        
        productController.fetchFromServer = true
        productController.product = tmpProduct
        
        let navController = UINavigationController(rootViewController: productController)
        self.present(navController, animated: true, completion: nil)
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return promotion.PromotionsProducts.count
    }
    
    @IBAction func toBasketBtnAction(_ sender: Any) {
        
        _ = BasketHandler().handleBasket(handle: .append, current_product: promotion)
    }
    
    @IBAction func prependProductBtn(_ sender: Any) {
        
        _ = BasketHandler().handleBasket(handle: .prepend, current_product: promotion)
    }
    
    
    @IBAction func appendProductBtn(_ sender: Any) {
        
        _ = BasketHandler().handleBasket(handle: .append, current_product: promotion)
    }
    
    @objc func updateBasketUI(){
        UIView.transition(with: view,
                          duration: 0.15,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.initView()
                            self.basketBtn.setBadge(value: BasketHandler().getCurrentBasketTotalCount()) })
    }
    
    func openBasket(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "BasketView", bundle: nil)
        let productController = storyBoard.instantiateViewController(withIdentifier: "BasketVC") as! BasketVC
        
        let navController = UINavigationController(rootViewController: productController)
        self.present(navController, animated: true, completion: nil)
    }
}
