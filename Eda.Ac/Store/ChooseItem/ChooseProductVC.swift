//
//  ChooseProductVC.swift
//  Eda.Ac
//
//  Created by dingo on 05.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import SwiftyJSON
import YTBarButtonItemWithBadge

class ChooseProductVC : UIViewController, UIScrollViewDelegate {
    
    var product : Product!
    
    //UI Controllers
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productDesc: UILabel!
    @IBOutlet weak var productComposition: UILabel!
    @IBOutlet weak var productWeight: UILabel!
    @IBOutlet weak var productCategory: UILabel!
    @IBOutlet weak var toBasketBtn: UIButton!
    @IBOutlet weak var toBasketManageBtnsView: UIView!
    @IBOutlet weak var toBasketCountLabel: UILabel!
    
    let basketBtn = YTBarButtonItemWithBadge();
    
    //FLAG
    var fetchFromServer = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: mainColor]
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "arrow"), style: .plain, target: self, action: #selector(hideVC))
        navigationController?.navigationBar.tintColor = mainColor
        
        self.scrollView.delegate = self
        
        if (product == nil){
            self.dismiss(animated: true)
        } else {
            if fetchFromServer{
                fetchDataFromServer()
            }else{
                initView()
            }
        }
        
        //Create basket badge
        basketBtn.setHandler(callback: openBasket);
        basketBtn.setImage(image: #imageLiteral(resourceName: "backet"));
        basketBtn.setBadge(value: BasketHandler().getCurrentBasketTotalCount());
        self.navigationItem.setRightBarButton(basketBtn.getBarButtonItem(), animated: true);
        
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.updateBasketUI), name: .basketChanged, object: nil)
    }
    

    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = scrollView.contentOffset.y
        
        print(y)
        
        if y > -50 {
            UIView.animate(withDuration: 0.3){
                self.navigationController?.view.backgroundColor = UIColor.clear
                self.navigationController?.navigationBar.isTranslucent = false

            }
        } else {
            UIView.animate(withDuration: 0.3){
                self.navigationController?.view.backgroundColor = UIColor.white
                self.navigationController?.navigationBar.isTranslucent = false
            }
        }
    }
    
    @objc func hideVC(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func fetchDataFromServer(){
        var url = serverApiURL + "products"
        
        let currentStoreId = BasketHandler().getCurrentStoreId()
        
        url += "/\(self.product.id)?s_id=\(currentStoreId)"
        
        Alamofire.request(url).responseJSON { response in
            if let result = response.result.value {
                let json = JSON(result)
                
                var _product = Product(.Product)
                
                _product.id = json["id"].stringValue
                _product.desc = json["desc"].stringValue
                _product.composition = json["composition"].stringValue
                _product.imageURL = json["image"].stringValue
                _product.weight = json["weight"].stringValue
                _product.price = json["price"].floatValue
                _product.title = json["title"].stringValue
                _product.categoryLabel = json["category_title"].stringValue
                _product.categoryId = json["category_id"].stringValue
                _product.storeId = currentStoreId
                
                self.product = _product
                
                
                self.initView()
                
                self.fetchFromServer = false
            }
        }
    }
    
    func initView(){
        title = product.title
        
        toBasketBtn.layer.cornerRadius = toBasketBtn.layer.frame.height / 2
        
        toBasketManageBtnsView.layer.borderColor = mainColor.cgColor
        toBasketManageBtnsView.layer.borderWidth = 1.0
        toBasketManageBtnsView.layer.cornerRadius = toBasketManageBtnsView.layer.frame.height / 2
        toBasketManageBtnsView.layer.masksToBounds = true
        toBasketManageBtnsView.isHidden = true
        
        if let url = URL(string: serverImageUrl + product.imageURL) {
            productImage.af_setImage(withURL: url)
        }
        productTitle.text = product.title
        productPrice.text = "\(product.price) \(roubleSymbol)"
        productDesc.text = product.desc
        productComposition.text = product.composition
        productWeight.text = "\(product.weight) гр."
        productCategory.text = product.categoryLabel
        
        self.basketBtn.setBadge(value: BasketHandler().getCurrentBasketTotalCount());
        
        let count = BasketHandler().productCountInBasket(product_id: product.id)
        
        if count == 0 {
            //Его нету в корзине
            toBasketManageBtnsView.isHidden = true
            toBasketBtn.isHidden = false
        } else {
            //Он в корзине
            toBasketManageBtnsView.isHidden = false
            toBasketBtn.isHidden = true
            
            toBasketCountLabel.text = "\(count) шт. \(Float(count) * product.price) \(roubleSymbol)"
        }
    }
    
    @IBAction func toBasketBtnAction(_ sender: Any) {
        _ = BasketHandler().handleBasket(handle: .append, current_product: product)
    }
    
    @IBAction func prependProductBtn(_ sender: Any) {
        _ = BasketHandler().handleBasket(handle: .prepend, current_product: product)
    }
    
    
    @IBAction func appendProductBtn(_ sender: Any) {

        _ = BasketHandler().handleBasket(handle: .append, current_product: product)
    }
    
    @objc func updateBasketUI(){
        UIView.transition(with: view,
                          duration: 0.15,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.initView()
                            self.basketBtn.setBadge(value: BasketHandler().getCurrentBasketTotalCount());
        })
    }
    
    func openBasket(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "BasketView", bundle: nil)
        let productController = storyBoard.instantiateViewController(withIdentifier: "BasketVC") as! BasketVC
        
        let navController = UINavigationController(rootViewController: productController)
        self.present(navController, animated: true, completion: nil)
    }
}
