//
//  StorePromotions.swift
//  Eda.Ac
//
//  Created by dingo on 01.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON

class StorePromotionsVC : UITableViewController {
    
    var promotions = [Product]()
    var StoreID = String()
    var delegate : StoreViewControllerDelegate!
    let notificationCenter = NotificationCenter.default
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        getPromotions()
        
        notificationCenter.addObserver(self, selector: #selector(self.updateBasketUI), name: .basketChanged, object: nil)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PromotionCell") as! PromotionCell
        
        let product = self.promotions[indexPath.row]
        
        cell.promotionTitle.text = product.title
        cell.promotionDesc.text = product.desc
        if let url = URL(string: serverImageUrl + product.imageURL){
            cell.promotionImage.af_setImage(withURL: url)
        }
        cell.promotionPrice.text = "\(product.price) \(roubleSymbol)"
        
        let count = BasketHandler().productCountInBasket(product_id: product.id)
        
        if count != 0 {
            //Он уже в корзине
            cell.promotionManageBtnsView.isHidden = false
            cell.promotionToBasketBtn.isHidden = true
            
            cell.promotionBasketCountLabel.text = "\(count) шт. \(product.price * Float(count)) \(roubleSymbol)"
        }else{
            //абас бля
            cell.promotionManageBtnsView.isHidden = true
            cell.promotionToBasketBtn.isHidden = false
        }
        
        cell.appendToBasketBtn.tag = indexPath.row
        cell.promotionToBasketBtn.tag = indexPath.row
        cell.prependFromBasketBtn.tag = indexPath.row
        
        
        cell.appendToBasketBtn.addTarget(self, action: #selector(AppendToBasket(_:)), for: .touchUpInside)
        cell.prependFromBasketBtn.addTarget(self, action: #selector(PrependBasket(_:)), for: .touchUpInside)
        cell.promotionToBasketBtn.addTarget(self, action: #selector(AppendToBasket(_:)), for: .touchUpInside)
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Products", bundle: nil)
        let promotionController = storyBoard.instantiateViewController(withIdentifier: "ChoosePromotionVC") as! ChoosePromotionVC
        
        
        promotionController.promotion = self.promotions[indexPath.row]
        
        let navController = UINavigationController(rootViewController: promotionController)
        self.present(navController, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return promotions.count
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = scrollView.contentOffset.y
        delegate.scrolling(distance: y)
    }
    
    @objc func AppendToBasket(_ sender : UIButton) {
        notificationCenter.removeObserver(self, name: .basketChanged, object: nil)

        if BasketHandler().handleBasket(handle: .append, current_product: self.promotions[sender.tag]) {
            reloadUI(sender.tag, .append)
        }
        
        notificationCenter.addObserver(self, selector: #selector(self.updateBasketUI), name: .basketChanged, object: nil)
    }
    
    @objc func PrependBasket(_ sender : UIButton) {
        notificationCenter.removeObserver(self, name: .basketChanged, object: nil)

        if BasketHandler().handleBasket(handle: .prepend, current_product: self.promotions[sender.tag]) {
            reloadUI(sender.tag, .prepend)
        }
        
        notificationCenter.addObserver(self, selector: #selector(self.updateBasketUI), name: .basketChanged, object: nil)

    }
    
    func reloadUI(_ id : Int, _ handleType : basketHandleType){

        
        let indexPath = IndexPath(row: id, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as! PromotionCell
        let count = BasketHandler().productCountInBasket(product_id: self.promotions[id].id)

        
        UIView.transition(with: tableView,
                          duration: 0.15,
                          options: .transitionCrossDissolve,
                          animations: {
                            
                            if count == 0 {
                                //Убрать нахуй
                                cell.promotionManageBtnsView.isHidden = true
                                cell.promotionToBasketBtn.isHidden = false
                            } else {
                                //Подумать
                                cell.promotionManageBtnsView.isHidden = false
                                cell.promotionToBasketBtn.isHidden = true
                                
                                cell.promotionBasketCountLabel.text = "\(count) шт. \(self.promotions[id].price * Float(count)) \(roubleSymbol)"
                            }
        })
        
    }
    
    func getPromotions(){
        self.promotions.removeAll()
        var url = serverApiURL + "promotions"
        
        url += "?s_id=\(StoreID)"
        
        Alamofire.request(url).responseJSON { response in
            if let result = response.result.value {
                let json = JSON(result)
                
                for index in 0..<json.arrayValue.count {
                    let promotion_obj = json[index]
                    //Инициализация внутренних продуктов
                    var _promotions_products = [Product]()
                    
                    let promotionProducts = promotion_obj["promotionProducts"].arrayValue
                    
                    for index in 0..<promotionProducts.count {
                        let promotionProduct = promotionProducts[index]
                        var product = Product(.Product)
                        
                        product.id = promotionProduct["p_id"].stringValue
                        product.title = promotionProduct["title"].stringValue
                        product.desc = promotionProduct["composition"].stringValue
                        product.imageURL = promotionProduct["image"].stringValue
                        product.storeId = self.StoreID
                        
                        _promotions_products.append(product)
                        
                    }
                    
                    //Инициализация акции
                    var promotion = Product(.Promotion)
                    
                    promotion.id = promotion_obj["id"].stringValue
                    promotion.title = promotion_obj["title"].stringValue
                    promotion.desc = promotion_obj["desc"].stringValue
                    promotion.price = promotion_obj["price"].floatValue
                    promotion.imageURL = promotion_obj["image"].stringValue
                    promotion.storeId = self.StoreID
                    promotion.PromotionsProducts = _promotions_products
                    
                    self.promotions.append(promotion)
                }
                
                self.tableView.reloadData()
            }
        }
    }
    
    @objc func updateBasketUI(){
        getPromotions()
    }
}
