//
//  StoreProducts.swift
//  Eda.Ac
//
//  Created by dingo on 02.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
import CoreData
import YTBarButtonItemWithBadge

class StoreProducts : UITableViewController {
    
    var StoreID = String()
    
    var Category = productCategory()
    
    var products = [Product]()
    
    var delegate : StoreViewControllerChildDelegate!
    
    let basketBtn = YTBarButtonItemWithBadge();

    let notificationCenter = NotificationCenter.default

    override func viewDidLoad() {
        super.viewDidLoad()
        getProducts()
        
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "arrow"), style: .plain, target: self, action: #selector(hideVC))
        navigationController?.navigationBar.tintColor = mainColor
        title = Category.title
        
        
        //Create basket badge
        basketBtn.setHandler(callback: openBasket);
        basketBtn.setImage(image: #imageLiteral(resourceName: "backet"));
        basketBtn.setBadge(value: BasketHandler().getCurrentBasketTotalCount());
        self.navigationItem.setRightBarButton(basketBtn.getBarButtonItem(), animated: true);
        
        notificationCenter.addObserver(self, selector: #selector(self.updateBasketUI), name: .basketChanged, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        updateBasketUI()
    }
    
    @objc func hideVC(){
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        delegate.childViewClosed()
    }
    
    func getProducts(){
        var url = serverApiURL + "products"
        
        url += "?s_id=\(self.StoreID)&cat_id=\(self.Category.id)"
        
        Alamofire.request(url).responseJSON { response in
            if let result = response.result.value {
                let json = JSON(result)
                
                self.products.removeAll()
                
                for index in 0..<json.arrayValue.count {
                    
                    let ProductObject = json[index]
                    
                    var product = Product(.Product)
                    
                    product.id = ProductObject["id"].stringValue
                    product.desc = ProductObject["desc"].stringValue
                    product.composition = ProductObject["composition"].stringValue
                    product.imageURL = ProductObject["image"].stringValue
                    product.weight = ProductObject["weight"].stringValue
                    product.price = ProductObject["price"].floatValue
                    product.title = ProductObject["title"].stringValue
                    product.categoryLabel = ProductObject["category_title"].stringValue
                    product.categoryId = ProductObject["category_id"].stringValue
                    product.storeId = self.StoreID
                    
                    self.products.append(product)
                }
                
                //reload tableView
                self.tableView.reloadData()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StoreProductsTableViewCell") as! StoreProductsTableViewCell
        
        let product = self.products[indexPath.row]

        
        if let url = URL(string: serverImageUrl + product.imageURL){
            cell.productImage.af_setImage(withURL: url)
        }
        
        cell.productDesc.text = product.desc
        cell.productTitle.text = product.title
        cell.productPriceLabel.text = "\(product.price) \(roubleSymbol)"
        
        cell.productToBasketBtn.tag = indexPath.row
        cell.productPrependCount.tag = indexPath.row
        cell.productAppendCount.tag = indexPath.row
        
        //Проверка если он есть товар в корзине
        
        let count = BasketHandler().productCountInBasket(product_id: product.id)
        
        if count != 0 {
            //Товар в корзине

            cell.productAppendBasketView.isHidden = false
            cell.productToBasketBtn.isHidden = true

        } else {
            //нихуя нету
            cell.productAppendBasketView.isHidden = true
            cell.productToBasketBtn.isHidden = false

        }
        
        cell.productInBasketCountLabel.text = "\(count) шт. \(product.price * Float(count)) \(roubleSymbol)"
        
        cell.productToBasketBtn.addTarget(self, action: #selector(AppendToBasket(_:)), for: .touchUpInside)
        cell.productPrependCount.addTarget(self, action: #selector(PrependBasket(_:)), for: .touchUpInside)
        cell.productAppendCount.addTarget(self, action: #selector(AppendToBasket(_:)), for: .touchUpInside)
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Products", bundle: nil)
        let productController = storyBoard.instantiateViewController(withIdentifier: "ChooseProductVC") as! ChooseProductVC
        productController.product = self.products[indexPath.row]
        
        let navController = UINavigationController(rootViewController: productController)
        self.present(navController, animated: true, completion: nil)
    }
    
    //BASKET LOGIC
    
    @objc func AppendToBasket(_ sender : UIButton) {
    
        notificationCenter.removeObserver(self, name: .basketChanged, object: nil)
        
        if BasketHandler().handleBasket(handle: .append, current_product: self.products[sender.tag]) {
            reloadUI(sender.tag, .prepend)
        }
        
        notificationCenter.addObserver(self, selector: #selector(self.updateBasketUI), name: .basketChanged, object: nil)
    }
    
    @objc func PrependBasket(_ sender : UIButton) {
        notificationCenter.removeObserver(self, name: .basketChanged, object: nil)
        
        if BasketHandler().handleBasket(handle: .prepend, current_product: self.products[sender.tag]) {
            reloadUI(sender.tag, .prepend)
        }
        
        notificationCenter.addObserver(self, selector: #selector(self.updateBasketUI), name: .basketChanged, object: nil)
    }
    
    func reloadUI(_ id : Int, _ handleType : basketHandleType){
        
        
        let indexPath = IndexPath(row: id, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as! StoreProductsTableViewCell
        let count = BasketHandler().productCountInBasket(product_id: self.products[id].id)
        
        
        UIView.transition(with: tableView,
                          duration: 0.15,
                          options: .transitionCrossDissolve,
                          animations: {
                            
                            if count == 0 {
                                //Убрать нахуй
                                cell.productAppendBasketView.isHidden = true
                                cell.productToBasketBtn.isHidden = false
                            } else {
                                //Подумать

                                cell.productAppendBasketView.isHidden = false
                                cell.productToBasketBtn.isHidden = true
                                
                                cell.productInBasketCountLabel.text = "\(count) шт. \(self.products[id].price * Float(count)) \(roubleSymbol)"
                            }
                            self.basketBtn.setBadge(value: BasketHandler().getCurrentBasketTotalCount());
        })
        
    }
    
    @objc func updateBasketUI(){
        getProducts()
        basketBtn.setBadge(value: BasketHandler().getCurrentBasketTotalCount());
    }
    
    func openBasket(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "BasketView", bundle: nil)
        let productController = storyBoard.instantiateViewController(withIdentifier: "BasketVC") as! BasketVC
        
        let navController = UINavigationController(rootViewController: productController)
        self.present(navController, animated: true, completion: nil)
    }
}


