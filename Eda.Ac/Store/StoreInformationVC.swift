//
//  StoreInformationVC.swift
//  Eda.Ac
//
//  Created by dingo on 01.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AACarousel
import GoogleMaps

struct storeImage {
    var imageURL : String
    var Title : String
}

class StoreInformationVC : UIViewController, AACarouselDelegate, UIScrollViewDelegate {
    
    var Store = StoreInfo()
    
    var delegate : StoreViewControllerDelegate!
    
    @IBOutlet weak var slider: AACarousel!
    @IBOutlet weak var map: GMSMapView!
    @IBOutlet weak var aboutStoreLabel: UILabel!
    @IBOutlet weak var workTime: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    var imageURLs = [storeImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        slider.delegate = self
        scrollView.delegate = self
        
        initView()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = scrollView.contentOffset.y
        
        delegate.scrolling(distance: y)
    }
    
    
    func initView(){
        
        map.layer.cornerRadius = 10.0
        map.layer.masksToBounds = true
        
        
        
        aboutStoreLabel.text = Store.desc
        workTime.text = "\(Store.hours_from) - \(Store.hours_to)"
        
        
        initMap()
        
        getImages()
    }
    
    //MAP
    func initMap(){
        if !Store.lat.isEmpty && !Store.lon.isEmpty {
            if let lat = NumberFormatter().number(from: Store.lat) {
                if let lon = NumberFormatter().number(from: Store.lon) {
                    let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(truncating: lat),
                                                          longitude: CLLocationDegrees(truncating: lon) ,
                                                          zoom: 16.0)
                    map.animate(to: camera)
                    
                    //map.isMyLocationEnabled = true
                    
                    let marker = GMSMarker()
                    marker.position = CLLocationCoordinate2D(latitude: CLLocationDegrees(CGFloat(truncating: lat)),
                                                             longitude: CLLocationDegrees(CGFloat(truncating: lon)))
                    marker.title = Store.title
                    marker.snippet = Store.address
                    marker.map = map
                }
            }
        }
    }
    
    
    //CAROUSEL
    func didSelectCarouselView(_ view: AACarousel, _ index: Int) {
        
    }
    
    func callBackFirstDisplayView(_ imageView: UIImageView, _ url: [String], _ index: Int) {
        
    }
    
    func downloadImages(_ url: String, _ index: Int) {
        
        Alamofire.request(url).responseData { response in
            guard let data = response.result.value else { return }
            if let image = UIImage(data: data) {
                self.slider.images[index] = image
            }
        }
        
        //    self.carouselView.images[index] = downloadImage!
        
    }
    
    func getImages(){
        var url = serverApiURL + "get-store-images"
        
        url += "?s_id=\(self.Store.id)"
        
        Alamofire.request(url).responseJSON { response in
            if let result = response.result.value {
                let json = JSON(result)
                
                for index in 0..<json.arrayValue.count {
                    
                    let image_url = serverImageUrl + json[index]["image"].stringValue
                    let image_title = json[index]["title"].stringValue
                    
                    let store_image = storeImage.init(imageURL: image_url,
                                                      Title: image_title)
                    
                    self.imageURLs.append(store_image)
                }
                                
                //
                self.slider.layer.cornerRadius = 20.0
                self.slider.layer.masksToBounds = true
                
                self.slider.setCarouselData(paths: self.imageURLs.map { $0.imageURL },  describedTitle: self.imageURLs.map { $0.Title }, isAutoScroll: true, timer: 5.0, defaultImage: "defaultImage")
                self.slider.setCarouselLayout(displayStyle: 0, pageIndicatorPositon: 2, pageIndicatorColor: nil, describedTitleColor: UIColor.clear, layerColor: nil)
                
            }
        }
    }
}
