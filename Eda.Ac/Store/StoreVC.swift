//
//  StoreVC.swift
//  Eda.Ac
//
//  Created by dingo on 01.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
import YTBarButtonItemWithBadge

class StoreVC : UIViewController, StoreViewControllerDelegate {
    
    @IBOutlet weak var indicatorRightSpace: NSLayoutConstraint!
    @IBOutlet weak var indicatorLeftSpace: NSLayoutConstraint!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var discountsBtn: UIButton!
    @IBOutlet weak var infoBtn: UIButton!
    
    @IBOutlet weak var headerView: UIView!
    
    //Store UI Elements
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var PayTypeLabel: UILabel!
    @IBOutlet weak var WorkTimeLabel: UILabel!
    @IBOutlet weak var minPriceLabel: UILabel!
    @IBOutlet weak var DeliveryLabel: UILabel!
    @IBOutlet weak var MiddleTimeLabel: UILabel!
    @IBOutlet weak var blurEffect: UIVisualEffectView!
    let basketBtn = YTBarButtonItemWithBadge();

    
    //Store pages
    @IBOutlet weak var storeInformationContainer: UIView!
    @IBOutlet weak var storePromotionsContainer: UIView!
    @IBOutlet weak var storeMenuVC: UIView!
    
    
    var fullWidth = CGFloat()
    var fullHeight = CGFloat()
    var modalScrolledDistance : CGFloat = 0
    
    
    var loadIndex = 0
    
    var Store = StoreInfo()
    
    var gradientLayer: CAGradientLayer!

    var navbarheight = CGFloat()
    
    
    //
    var ProductCategories = [productCategory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: mainColor]

        
        fullWidth = self.view.frame.width
        fullHeight = self.view.frame.height
        
        self.view.frame = CGRect(x: 0, y: 0, width: fullWidth, height: fullWidth+250)
        
        selectHost(id: 0)
        blurEffect.alpha = 1.0

        loadStoreImages()
        loadMenuList()
        
        //Create basket badge
        basketBtn.setHandler(callback: openBasket);
        basketBtn.setImage(image: #imageLiteral(resourceName: "backet"));
        basketBtn.setBadge(value: BasketHandler().getCurrentBasketTotalCount());
        self.navigationItem.setRightBarButton(basketBtn.getBarButtonItem(), animated: true);
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.updateBasketUI), name: .basketChanged, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        initView()
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        UIView.animate(withDuration: 0.2){
            self.headerView.alpha = 0.0
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func openMenu(_ sender: Any) {
        selectHost(id: 0)
    }
    
    @IBAction func openDiscounts(_ sender: Any) {
        selectHost(id: 1)
    }
    
    @IBAction func openInfo(_ sender: Any) {
        selectHost(id: 2)
    }
    
    
    func selectHost(id : Int){
        //TODO : SCROLL TO TOP WHEN BUTTON TAPPED
        
        var rightValue = CGFloat(0)
        var leftValue = CGFloat(0)
        
        switch id {
        case 0:
            rightValue = (fullWidth / 3) * 2
            leftValue = (fullWidth / 3) * 0
            
            self.menuBtn.setTitleColor(mainColor, for: .normal)
            self.discountsBtn.setTitleColor(UIColor.white, for: .normal)
            self.infoBtn.setTitleColor(UIColor.white, for: .normal)
            
            self.view.frame = CGRect(x: 0, y: 0, width: fullWidth, height: 44*80)

            
            UIView.animate(withDuration: 0.3){
                self.storePromotionsContainer.isHidden = true
                self.storeInformationContainer.isHidden = true
                self.storeMenuVC.isHidden = false
            }
            break
        case 1:
            self.menuBtn.setTitleColor(UIColor.white, for: .normal)
            self.discountsBtn.setTitleColor(mainColor, for: .normal)
            self.infoBtn.setTitleColor(UIColor.white, for: .normal)
            rightValue = (fullWidth / 3) * 1
            leftValue = (fullWidth / 3) * 1
            
            UIView.animate(withDuration: 0.3){
                self.storePromotionsContainer.isHidden = false
                self.storeInformationContainer.isHidden = true
                self.storeMenuVC.isHidden = true
            }
            break
        case 2:
            self.menuBtn.setTitleColor(UIColor.white, for: .normal)
            self.discountsBtn.setTitleColor(UIColor.white, for: .normal)
            self.infoBtn.setTitleColor(mainColor, for: .normal)
            rightValue = (fullWidth / 3) * 0
            leftValue = (fullWidth / 3) * 2
            
            UIView.animate(withDuration: 0.3){
                self.storePromotionsContainer.isHidden = true
                self.storeInformationContainer.isHidden = false
                self.storeMenuVC.isHidden = true
            }
            break
        default: break
        }
        
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.1){
            self.indicatorRightSpace.constant = rightValue
            self.indicatorLeftSpace.constant = leftValue
            self.view.layoutIfNeeded()

        }
    }
    
    func finishLoad(){
        //0: StoreInformationVC
        let informationVC = childViewControllers[0] as? StoreInformationVC
        informationVC?.Store = self.Store
        informationVC?.delegate = self
        informationVC?.initView()
        //1: StorePromotionsVC
        let promotionsVC = childViewControllers[1] as? StorePromotionsVC
        promotionsVC?.StoreID = self.Store.id
        promotionsVC?.delegate = self
        promotionsVC?.getPromotions()
        //2: StoreMenuVC

        let menuVC = childViewControllers[2] as? StoreMenuVC
        menuVC?.menus = ProductCategories
        menuVC?.tableView.reloadData()
        
        loadIndex += 1
        
        if loadIndex == 3 {
            //hide spinner
            
        }
    }
    
    
    //send delegate
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "toStoreMenuVC") {
            let menuVC = segue.destination as! StoreMenuVC
            
            menuVC.menus = ProductCategories
            menuVC.delegate = self
        }
    }
    
    
    func scrolling(distance: CGFloat) {
        self.modalScrolledDistance = distance
        
        
        handleHeader()
    }
    
    func handleHeader(){
        
        if modalScrolledDistance <= 300-navbarheight{
            //show nav bar
            UIView.animate(withDuration: 0.3){
                self.title = ""
            }
            
            blurEffect.alpha = (modalScrolledDistance * 1.5 / 1000) * 3
            
            self.view.frame = CGRect(x: 0, y: 0 - modalScrolledDistance, width: self.view.frame.size.width, height: self.fullHeight + modalScrolledDistance );
        } else {
            //hide nav bar
            UIView.animate(withDuration: 0.1){
                self.title = self.Store.title
                self.blurEffect.alpha = (self.modalScrolledDistance * 1.5 / 1000) * 3
                self.view.frame = CGRect(x: 0, y: 0 - 300 + self.navbarheight, width: self.view.frame.size.width, height: self.fullHeight + 300 - self.navbarheight );
            }
        }
    }
    
    @objc func backButton(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func initView(){
        UIView.animate(withDuration: 0.2){
            self.headerView.alpha = 1.0
        }
        
        navbarheight = self.navigationController!.navigationBar.frame.size.height + UIApplication.shared.statusBarFrame.height
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: #selector(self.backButton))
        
        storeImage.layer.cornerRadius = 20.0
        storeImage.layer.masksToBounds = true
        storeImage.layer.borderColor = UIColor.white.cgColor
        storeImage.layer.borderWidth = 2.0
        
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.background.bounds
        gradientLayer.colors = [UIColor.clear.cgColor, #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 0.5).cgColor]
        
        self.background.layer.addSublayer(gradientLayer)
        
        self.PayTypeLabel.text = self.Store.payType == .Cash ? "Только наличная оплата" : "Оплата картой"
        self.WorkTimeLabel.text = self.Store.hours_from + " - " + self.Store.hours_to
        self.DeliveryLabel.text = self.Store.delivery
        self.minPriceLabel.text = "\(self.Store.min_price_order) " + roubleSymbol
        self.MiddleTimeLabel.text = "от " + self.Store.delivery_time + " мин"
        
        UIView.animate(withDuration: 0.2){
            self.blurEffect.alpha = 0.0
        }
        
    }
    
    
    
    func loadStoreImages(){
        if let url = URL(string: serverImageUrl + self.Store.background) {
            headerImage.af_setImage(withURL: url)
        }
        if let url = URL(string: serverImageUrl + self.Store.image) {
            storeImage.af_setImage(withURL: url)
        }
    }
    
    func loadMenuList(){
        var url = serverApiURL + "products-categories"
        
        url += "?s_id=\(self.Store.id)"
        
        Alamofire.request(url).responseJSON { response in
            if let result = response.result.value {
                let json = JSON(result)
                
                for index in 0..<json.arrayValue.count {
                    
                    let id = json[index]["id"].stringValue
                    let title = json[index]["title"].stringValue
                    
                    
                    var product_category = productCategory.init()
                    
                    product_category.id = id
                    product_category.title = title
                    product_category.StoreID = self.Store.id
                    
                    self.ProductCategories.append(product_category)
                }
                
                self.finishLoad()
            }
        }
    }
    
    @objc func updateBasketUI(){
        basketBtn.setBadge(value: BasketHandler().getCurrentBasketTotalCount());
    }
    
    @objc func openBasket(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "BasketView", bundle: nil)
        let productController = storyBoard.instantiateViewController(withIdentifier: "BasketVC") as! BasketVC
        
        let navController = UINavigationController(rootViewController: productController)
        self.present(navController, animated: true, completion: nil)
    }
}
