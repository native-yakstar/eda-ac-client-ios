//
//  StoreMenuVC.swift
//  Eda.Ac
//
//  Created by dingo on 01.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit

class StoreMenuVC : UITableViewController, StoreViewControllerChildDelegate {
    
    func childViewClosed() {
        //scroll to top
        
        self.tableView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    var delegate : StoreViewControllerDelegate!
    var StoreID = String()
    var menus = [productCategory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorColor = UIColor.clear
        self.tableView.separatorStyle = .none
        
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "store_menu_cell")
        cell!.layer.cornerRadius = 20
        cell!.layer.masksToBounds = true
        
        cell?.textLabel?.text = menus[indexPath.row].title
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        //show cat products
        let storyBoard: UIStoryboard = UIStoryboard(name: "Products", bundle: nil)
        let productsController = storyBoard.instantiateViewController(withIdentifier: "StoreProducts") as! StoreProducts
        productsController.StoreID = self.menus[indexPath.row].StoreID
        productsController.Category = self.menus[indexPath.row]
        productsController.delegate = self
        
        let navController = UINavigationController(rootViewController: productsController)
        self.present(navController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = scrollView.contentOffset.y
        delegate.scrolling(distance: y)
    }
}
