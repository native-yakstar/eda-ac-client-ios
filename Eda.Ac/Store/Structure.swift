//
//  Structure.swift
//  Eda.Ac
//
//  Created by dingo on 01.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit

struct productCategory {
    var id : String
    var title : String
    var StoreID : String
    
    init() {
        id = String()
        title = String()
        StoreID = String()
    }
}

enum ProductType {
    case Product
    case Promotion
}

struct Promotion {
    var title : String
    var descr : String
    var price : Int
    var products : [Product]
    //admin
    var status : String
    
    init(){
        title = String()
        descr = String()
        price = 0
        products = [Product]()
        status = String()
    }
}

struct Product {
    var id : String
    var title : String
    var imageURL : String
    var desc : String
    var composition : String
    var weight : String
    var price : Float
    var categoryLabel : String
    var categoryId : String
    var storeId : String
    var type : ProductType
    var count : Int
    var PromotionsProducts : [Product]
    
    //admin
    var status : String
    
    init(_ productType : ProductType) {
        id = ""
        title = ""
        imageURL = ""
        desc = ""
        composition = ""
        weight = ""
        price = 0
        categoryLabel = ""
        categoryId = ""
        storeId = ""
        type = productType
        count = 0
        PromotionsProducts = [Product]()
        status = String()
    }
}

struct StoreInfo {
    var id : String
    var title : String
    var desc : String
    var background : String
    var delivery : String
    var image : String
    var hours_from : String
    var hours_to : String
    var address : String
    var payType : PayType
    var lat : String
    var lon : String
    var min_price_order : Int
    var delivery_time : String
    var category : [storeCatsStruct]
    
    init() {
        id = ""
        title = ""
        desc = ""
        background = ""
        delivery = ""
        image = ""
        hours_from = ""
        hours_to = ""
        address = ""
        payType = .AnyWay
        lat = ""
        lon = ""
        min_price_order = 0
        delivery_time = ""
        category = [storeCatsStruct]()
    }
}

protocol allDataIsLoaded {
    func dataLoaded()
}

protocol StoreViewControllerDelegate {
    func scrolling(distance : CGFloat)
}

protocol StoreViewControllerChildDelegate {
    func childViewClosed()
}
