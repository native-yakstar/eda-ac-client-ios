//
//  LegacyVC.swift
//  Eda.Ac
//
//  Created by dingo on 14.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit

class LegacyVC : UIViewController {
    
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = URL(string: legacyInfoURL) {
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
    }
    
    @IBAction func allow(_ sender: Any) {
        UserDefaults.standard.set("allowed", forKey: "legacy")
        UserDefaults.standard.synchronize()
        dismiss(animated: true, completion: nil)
    }
}
