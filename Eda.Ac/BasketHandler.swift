//
//  BasketHandler.swift
//  Eda.Ac
//
//  Created by dingo on 05.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import Foundation
import CoreData
import UIKit
import Alamofire
import SwiftyJSON

class BasketHandler {
    
    var appDelegate : AppDelegate!
    var context : NSManagedObjectContext!
    
    init() {
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
    }
    
    func productCountInBasket(product_id : String) -> Int {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Basket")
        request.predicate = NSPredicate(format: "id = %@",  product_id)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            
            if result.count != 0 {
                let product = result[0] as! NSManagedObject
                
                let current_count = product.value(forKey: "count") as! Int
                
                return current_count
            } else {
                return 0
            }
            
        } catch {
            print("Failed")
            
            return 0
        }
    }
    
    func productAvailableInBasket(product_id : String) -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Basket")
        request.predicate = NSPredicate(format: "id = %@", product_id)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            
            return result.count != 0
        } catch {
            print("Failed")
            return false
        }
    }
    
    func handleBasket(handle : basketHandleType, current_product : Product) -> Bool {
        
        if productFromAnotherStore(StoreID: current_product.storeId) == .AnotherStoreError {
            
            //Show alert view
            //Сказать пользователю чтобы очистил базу либо нахуй не покупал
            let topWindow = UIWindow(frame: UIScreen.main.bounds)
            topWindow.rootViewController = UIViewController()
            topWindow.windowLevel = (UIWindowLevelAlert + 1) as UIWindowLevel
            let alert = UIAlertController(title: "Внимание", message: "Вы пытаетесь добавить товар из другого магазина. Для того что бы добавить товар потребуется очистить корзину.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
                topWindow.isHidden = true
            }))
            alert.addAction(UIAlertAction(title: "Очистить корзину", style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
                self.clearBasket()
                topWindow.isHidden = true
            }))
            topWindow.makeKeyAndVisible()
            topWindow.rootViewController?.present(alert, animated: true)
            
            return false
        }
        
        if handle == .append {
            
            //Проверка есть ли он в базе
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Basket")
            request.predicate = NSPredicate(format: "id = %@", current_product.id)
            request.returnsObjectsAsFaults = false
            do {
                let result = try context.fetch(request)
                if result.count != 0 {
                    //Проверка на то что он не заказывает с другого магаза
                    
                    //Если есть то итерирую count
                    let product = result[0] as! NSManagedObject
                    
                    //get iterated count
                    let iterated_count = (product.value(forKey: "count") as! Int) + 1
                    
                    
                    product.setValue(iterated_count, forKey: "count")
                    
                    
                    do {
                        try context.save()
                        sendNotification()
                        
                        return true
                    } catch {
                        print("Failed saving")
                        return false
                    }
                    
                } else {
                    //Сделать магаз текущим
                    self.setDefaultStore(storeID: current_product.storeId)
                    
                    
                    //Иначе добавить
                    let entity = NSEntityDescription.entity(forEntityName: "Basket", in: context)
                    let basket = NSManagedObject(entity: entity!, insertInto: context)
                    
                    
                    basket.setValue(current_product.id, forKey: "id")
                    basket.setValue(current_product.title, forKey: "title")
                    basket.setValue(current_product.imageURL, forKey: "imageURL")
                    basket.setValue(current_product.desc, forKey: "desc")
                    basket.setValue(current_product.composition, forKey: "composition")
                    basket.setValue(current_product.weight, forKey: "weight")
                    basket.setValue(current_product.price, forKey: "price")
                    basket.setValue(current_product.categoryLabel, forKey: "categoryLabel")
                    basket.setValue(current_product.categoryId, forKey: "categoryId")
                    basket.setValue(current_product.storeId, forKey: "store_id")
                    basket.setValue(current_product.type == .Product ? 0 : 1, forKey: "type")
                    basket.setValue(1, forKey: "count")
                    
                    //Сохраняю данные
                    do {
                        try context.save()
                        sendNotification()
                        return true
                    } catch {
                        print("Failed saving")
                        return false
                    }
                    
                }
            } catch {
                print("Failed")
                return false
            }
        } else if handle == .prepend {
            //Убывание
            
            //Проверка есть ли он в базе
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Basket")
            request.predicate = NSPredicate(format: "id = %@", current_product.id)
            request.returnsObjectsAsFaults = false
            do {
                let result = try context.fetch(request)
                if result.count != 0 {
                    //Получение товара
                    let product = result[0] as! NSManagedObject
                    
                    //get current count
                    let current_count = product.value(forKey: "count") as! Int
                    
                    if current_count == 1 {
                        //Убрать объект с базы
                        context.delete(product)
                    }else {
                        //Убавить на 1 count
                        product.setValue(current_count - 1, forKey: "count")
                    }
                    
                    //Сохраняю данные
                    do {
                        try context.save()
                        sendNotification()
                        return true
                    } catch {
                        print("Failed saving")
                        return false
                    }
                    
                    
                } else {
                    //Невозможно убавить продукт его нет в базе
                    print("Невозможно убавить продукт его нет в базе")
                    return false
                }
            } catch {
                print("Failed")
                return false
            }
            
        } else {
            //Убрать
            
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Basket")
            request.predicate = NSPredicate(format: "id = %@", current_product.id)
            request.returnsObjectsAsFaults = false
            do {
                let result = try context.fetch(request)
                if result.count != 0 {
                    //Получение товара
                    let product = result[0] as! NSManagedObject
                    //Убрать объект с базы
                    context.delete(product)

                    //Сохраняю данные
                    do {
                        try context.save()
                        sendNotification()
                        return true
                    } catch {
                        print("Failed saving")
                        return false
                    }
                    
                } else {
                    //Невозможно убавить продукт его нет в базе
                    print("Невозможно убрать продукт его нет в базе")
                    return false
                }
            } catch {
                print("Failed")
                return false
            }
        }
    }
    
    func setDefaultStore(storeID : String){
        //Get Store Info
        var url = serverApiURL + "stores"
        
        url += "/\(storeID)"
        
        Alamofire.request(url).responseJSON { response in
            if let result = response.result.value {
                let json = JSON(result)
                
                let storeMinPrice = json["min_price_order"].intValue
                
                UserDefaults.standard.set(storeMinPrice, forKey: "store_min_price_order")
                UserDefaults.standard.set(storeID, forKey: "store_id")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    func getCurrentStoreId() -> String {
        if let CurrentStore = UserDefaults.standard.string(forKey: "store_id") {
            return CurrentStore
        } else {
            return "0"
        }
    }
    
    func getCurrentStoreMinPrice() -> Int {
        do {
            return UserDefaults.standard.integer(forKey: "store_min_price_order")
        }
    }
    
    func productFromAnotherStore(StoreID : String) -> basketErrors {
        //Сканирует всю базу если находит что store_id не из корзины посылает сигнал
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Basket")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            
            for index in 0..<result.count {
                let product = result[index] as! NSManagedObject
                
                if product.value(forKey: "store_id") as! String != StoreID {
                    return .AnotherStoreError
                }
            }
            
            return .NoErrors
        } catch {
            print("Failed")
            return .NoErrors
        }
    }
    
    func getCurrentBasketTotalCount() -> Float {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Basket")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            
            var TotalCount : Float = 0
            
            for index in 0..<result.count {
                let product = result[index] as! NSManagedObject

                //get count
                let count = product.value(forKey: "count") as! Int
                
                //get price
                let price = product.value(forKey: "price") as! Float
                
                let productCount = Float(count) * price
                
                TotalCount += productCount
            }
            return TotalCount
        } catch {
            print("Failed")
            return 0
        }
    }
    
    func getProductsInBasket() -> [Product] {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Basket")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            
            var products = [Product]()
            
            for index in 0..<result.count {
                let product = result[index] as! NSManagedObject

                var _product = Product(.Product)
                
                _product.id = product.value(forKey: "id") as! String
                _product.title = product.value(forKey: "title") as! String
                _product.imageURL = product.value(forKey: "imageURL") as! String
                _product.desc = product.value(forKey: "desc") as! String
                _product.composition = product.value(forKey: "composition") as! String
                _product.weight = product.value(forKey: "weight") as! String
                _product.price = product.value(forKey: "price") as! Float
                _product.categoryLabel = product.value(forKey: "categoryLabel") as! String
                _product.categoryId = product.value(forKey: "categoryId") as! String
                _product.storeId = product.value(forKey: "store_id") as! String
                _product.count = product.value(forKey: "count") as! Int
                _product.type = product.value(forKey: "type") as! Int == 0 ? .Product : .Promotion
                products.append(_product)
            }
            
            return products
            
        } catch {
            print("Failed")
            
            return [Product]()
        }
    }
    
    func clearBasket(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Basket")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: request)
        
        do {
            try context.execute(deleteRequest)
            sendNotification()
        } catch  {
            // TODO: handle the error
            print("Failed when clean basket")

        }
        
    }
    
    func sendNotification() {
        //Сообщить всем классам о том что корзина изменилась
        let notificationCenter = NotificationCenter.default
        notificationCenter.post(name: Notification.Name("basketChanged"), object: nil)
    }
}

extension Notification.Name {
    static let basketChanged = Notification.Name(rawValue: "basketChanged")
}

enum basketHandleType {
    case append
    case prepend
    case remove
}

enum basketErrors {
    case NoErrors
    case AnotherStoreError
}
