//
//  API.swift
//  Eda.Ac
//
//  Created by dingo on 12.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import Alamofire
import SwiftyJSON

struct adminFullPromotion {
    var id : String
    var desc : String
    var title : String
    var price : Float
    var status : String
    var image : String
    var products : [adminFullProduct]
    
    init() {
        id = String()
        desc = String()
        title = String()
        status = "1"
        image = String()
        price = 0.0
        products = [adminFullProduct]()
    }
}

class API {
    
    init(){
        print("API initialized")
    }
    
    //   IMAGE UPLOAD
    
    func uploadImage( image : UIImage, completion: @escaping (_ result : JSON) -> Void){
        
        let imageData = UIImageJPEGRepresentation(image, 0.5)!
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData, withName: "Upload[image]", fileName: "image.jpg", mimeType: "image/jpeg")
            
            print(multipartFormData.contentLength)
        },
                         to: serverApiURL + "panel/file?access-token=\(adminAccessToken)",
            headers: ["Content-Type":"multipart/form-data"],
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseData { response in
                        if let result = response.result.value {
                            
                            let obj = JSON(result)
                            
                            completion(obj)
                            
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    completion(JSON(["success" : false, "msg" : encodingError.localizedDescription]))
                    
                }
        })
    }
    
    
    //   PRODUCTS

    func adminUpdateProductToServer(product : adminFullProduct, completion: @escaping (_ result : JSON) -> Void){

        let parameters : Parameters = ["title" : product.title,
                                       "desc" : product.desc,
                                       "composition" : product.composition,
                                       "image" : product.image,
                                       "weight" : product.weight,
                                       "price" : product.price,
                                       "cat_id" : product.category_id]

        Alamofire.request(serverApiURL + "panel/products/update/" + product.id + "?access-token=" + adminAccessToken, method : .post, parameters : parameters).responseJSON { response in
            
            if let result = response.result.value {
                let json = JSON(result)

                completion(json)

            }
        }
    }
    
    
    func adminRemoveProduct(product : adminFullProduct, completion: @escaping (_ result : JSON) -> Void){
        
        
        Alamofire.request(serverApiURL + "panel/products/remove/" + product.id + "?access-token=" + adminAccessToken, method : .get).responseJSON { response in
            
            if let result = response.result.value {
                let json = JSON(result)
                
                completion(json)
                
            }
        }
    }
    
    func adminFetchProducts( completion: @escaping (_ products : [adminFullProduct]) -> Void){
        var Products = [adminFullProduct]()
        
        var url = serverApiURL + "panel/products"
        
        url += "?access-token=\(adminAccessToken)"
        
        
        Alamofire.request(url).responseJSON { response in
            if let result = response.result.value {
                let json = JSON(result)
                
                
                for product in json.arrayValue {
                    let id = product["id"].stringValue
                    let desc = product["desc"].stringValue
                    let composition = product["composition"].stringValue
                    let image = product["image"].stringValue
                    let weight = product["weight"].stringValue
                    let price = product["price"].floatValue
                    let title = product["title"].stringValue
                    let category_title = product["category_title"].stringValue
                    let category_id = product["category_id"].stringValue
                    
                    var productsObject = adminFullProduct()
                    
                    productsObject.id = id
                    productsObject.desc = desc
                    productsObject.composition = composition
                    productsObject.image = image
                    productsObject.weight = weight
                    productsObject.price = price
                    productsObject.title = title
                    productsObject.category_title = category_title
                    productsObject.category_id = category_id
                    
                    Products.append(productsObject)
                }
                
                completion(Products)
            }
        }
    }
    
    
    
    ///////// PROMOCODES
    
    func getPromocodes( completion: @escaping (_ promocodes : [PROMOCODE]) -> Void){
        var promocodes = [PROMOCODE]()
        
        var url = serverApiURL + "panel/promocodes"
        
        url += "?access-token=\(adminAccessToken)"
        
        Alamofire.request(url).responseJSON { response in
            if let result = response.result.value {
                let json = JSON(result)
                
                
                for promocode in json.arrayValue {
                    let id = promocode["id"].stringValue
                    let product_id = promocode["p_id"].stringValue
                    let product_title = promocode["p_title"].stringValue
                    let code = promocode["code"].stringValue
                    let percent = promocode["percent"].intValue
                    let discount = promocode["discount"].intValue
                    let type = self.identifyDiscount(id: promocode["type"].stringValue)
                    let typeString = promocode["type"].stringValue
                    let status = promocode["status"].stringValue == "1"
                    
                    var promocodeObject = PROMOCODE(type)
                    
                    promocodeObject.id = id
                    promocodeObject.p_id = product_id
                    promocodeObject.product_title = product_title
                    promocodeObject.code = code
                    promocodeObject.percent = percent
                    promocodeObject.discount = discount
                    promocodeObject.statusActive = status
                    promocodeObject.typeString = typeString
                    
                    promocodes.append(promocodeObject)
                }
                
                completion(promocodes)
            }
        }
    }
    
    func handlePromocode(promocode : PROMOCODE, handle : promocodeHandle, completion: @escaping (_ result : JSON) -> Void) {
        
        
        let parameters : Parameters = ["code" : promocode.code,
                                       "percent" : promocode.percent,
                                       "discount" : promocode.discount,
                                       "type" : promocode.typeString,
                                       "p_id" : promocode.p_id,
                                       "status" : promocode.statusActive ? "1" : "0"]
        
        var StringURL = String()
        switch handle {
        case .create:
            StringURL = serverApiURL + "panel/promocodes/insert?access-token=" + adminAccessToken
            break
        case .change:
            StringURL = serverApiURL + "panel/promocodes/update/\(promocode.id)?access-token=" + adminAccessToken
            break
        case .remove:
            StringURL = serverApiURL + "panel/promocodes/remove/\(promocode.id)?access-token=" + adminAccessToken
            break
        }
        
        Alamofire.request(StringURL, method : .post, parameters : parameters).responseJSON { response in
            
            if let result = response.result.value {
                let json = JSON(result)
                completion(json)
            }
        }
    }
    
    func identifyDiscount(id : String) -> promocodeType {
        
        switch id {
        case "1":
            return .productInGift
        case "2":
            return .DiscountForTotalCount
        case "3":
            return .DiscountForSpecificProduct
        default:
            return .Unknown
        }
    }
    
    
    
    //// PROMOTIONS
    
    func getProtions( completion: @escaping (_ promotions : [adminFullPromotion]) -> Void){
        var promotions = [adminFullPromotion]()
        
        var url = serverApiURL + "panel/promotions"
        
        url += "?access-token=\(adminAccessToken)"
        
        Alamofire.request(url).responseJSON { response in
            if let result = response.result.value {
                let json = JSON(result)
                
                
                for promotion in json.arrayValue {
                    
                    var promotionObject = adminFullPromotion()
                    
                    let id = promotion["id"].stringValue
                    let desc = promotion["desc"].stringValue
                    let title = promotion["title"].stringValue
                    let price = promotion["price"].floatValue
                    let status = promotion["status"].stringValue
                    let image = promotion["image"].stringValue
                    
                    promotionObject.id = id
                    promotionObject.desc = desc
                    promotionObject.title = title
                    promotionObject.price = price
                    promotionObject.status = status
                    promotionObject.image = image
                    
                    var products = [adminFullProduct]()
                    
                    for product in promotion["products"].arrayValue {
                        var productObject = adminFullProduct()
                        
                        productObject.id = product["id"].stringValue
                        productObject.title = product["title"].stringValue
                        productObject.composition = product["composition"].stringValue
                        productObject.image = product["image"].stringValue
                        
                        products.append(productObject)
                        
                    }
                    
                    promotionObject.products = products
                    

                    promotions.append(promotionObject)
                }
                
                completion(promotions)
            }
        }
    }
    
    func savePromotion(promotion : adminFullPromotion, handle : promotionHandle, completion: @escaping (_ result : JSON) -> Void){
        
        
        var ResponceJSON = JSON().dictionaryObject
        
        ResponceJSON = ["title" : promotion.title,
                        "desc" : promotion.desc,
                        "price" : promotion.price,
                        "products" : promotion.products.map { $0.id },
                        "status" : promotion.status]
        
        
        
        
        var urlString = String()
        
        switch handle {
        case .create:
            urlString = serverApiURL + "panel/promotions/insert?access-token=" + adminAccessToken
            break
        case .update:
            urlString = serverApiURL + "panel/promotions/update/\(promotion.id)?access-token=" + adminAccessToken
            break
        case .remove:
            urlString = serverApiURL + "panel/promotions/remove/\(promotion.id)?access-token=" + adminAccessToken
            break
        }
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        request.httpBody = try! JSON(ResponceJSON!).rawData()
        
        Alamofire.request(request).responseJSON { response in
                        
            if let result = response.result.value {
                let json = JSON(result)
                
                completion(json)
            }
        }
    }
    
    
    /// STORE
    
    func getStore( completion: @escaping (_ store : AdminStoreInfo) -> Void ) {
        
        Alamofire.request(serverApiURL + "panel/settings?access-token=" + adminAccessToken, method : .get).responseJSON { response in
            
            if let result = response.result.value {
                let json = JSON(result)
                
                var store = AdminStoreInfo()
                
                store.id = json["id"].stringValue
                store.title = json["title"].stringValue
                store.desc = json["desc"].stringValue
                store.delivery = json["delivery"].stringValue
                store.image = json["image"].stringValue
                store.hours_from = json["hours_from"].stringValue
                store.hours_to = json["hours_to"].stringValue
                store.address = json["address"].stringValue
                store.cashless_payment = json["cashless_payment"].stringValue
                store.lat = json["lat"].stringValue
                store.lon = json["lon"].stringValue
                store.min_price_order = json["min_price_order"].stringValue
                store.delivery_time = json["delivery_time"].stringValue
                store.background = json["background"].stringValue
                store.phone = json["phone"].stringValue

                completion(store)
                
            }
        }
    }
    
    func updateStore(store : AdminStoreInfo, completion: @escaping (_ result : JSON) -> Void ) {
        
        var ResponceJSON = JSON().dictionaryObject
        
        ResponceJSON = ["title" : store.title,
                        "desc" : store.desc,
                        "delivery" : store.delivery,
                        "image" : store.image,
                        "hours_from" : store.hours_from,
                        "hours_to" : store.hours_to,
                        "address" : store.address,
                        "cashless_payment" : store.cashless_payment,
                        "lat" : store.lat,
                        "lon" : store.lon,
                        "min_price_order" : store.min_price_order,
                        "delivery_time" : store.delivery_time,
                        "background" : store.background,
                        "phone" : store.phone]
        
        
        
        let url = URL(string: serverApiURL + "panel/settings/update/\(store.id)?access-token=\(adminAccessToken)")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        print(JSON(ResponceJSON!))
        
        request.httpBody = try! JSON(ResponceJSON!).rawData()
        
        Alamofire.request(request).responseJSON { response in
            
            if response.error != nil {
                completion(JSON(["success":false, "msg" : response.error!.localizedDescription ]))

            }
            
            if let result = response.result.value {
                let json = JSON(result)
                
                completion(json)
            }
        }
    }
    
    
    /// GALLERY
    
    func getStoreImages( completion: @escaping (_ images : [AdminStoreImage]) -> Void ) {
        
        Alamofire.request(serverApiURL + "panel/stores-gallery?access-token=" + adminAccessToken, method : .get).responseJSON { response in
            
            if let result = response.result.value {
                let json = JSON(result)
                
                var images = [AdminStoreImage]()
                
                for imageObj in json.arrayValue {
                    
                    var storeImage = AdminStoreImage()
                    
                    storeImage.id = imageObj["id"].stringValue
                    storeImage.title = imageObj["title"].stringValue
                    storeImage.image = imageObj["image"].stringValue
                    
                    images.append(storeImage)
                    
                }
                
                
                completion(images)
                
            }
        }
    }
    
    func removeStoreImages(image : AdminStoreImage, completion : @escaping (_ result : JSON) -> Void ){
        Alamofire.request(serverApiURL + "panel/stores-gallery/remove/\(image.id)?access-token=" + adminAccessToken, method : .get).responseJSON { response in
            
            if let result = response.result.value {
                let json = JSON(result)
                
                completion(json)
                
            }
        }
    }
    
    func insertStoreImages(image : AdminStoreImage, completion : @escaping (_ result : JSON) -> Void ){
        
        let parameters :  Parameters = ["image" : image.image,
                                        "title" : image.title]
        
        Alamofire.request(serverApiURL + "panel/stores-gallery/insert/?access-token=" + adminAccessToken, method : .post, parameters : parameters).responseJSON { response in
            
            if let result = response.result.value {
                let json = JSON(result)
                
                completion(json)
                
            }
        }
    }
}

enum promotionHandle {
    case create
    case update
    case remove
}

enum promocodeHandle {
    case create
    case change
    case remove
}
