//
//  FilterStoreParametersVC.swift
//  Eda.Ac
//
//  Created by dingo on 30.11.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit

class FilterStoreParametersVC : UITableViewController {
    
    var delegate : FilterStoreParamatersDelegate!

    var SelectedCells = [Bool]()
    
    var Parameters : [[String : Any]] = [["image" : #imageLiteral(resourceName: "cherry"),
                                        "title" : "Работает сейчас"],
                                        ["image" : #imageLiteral(resourceName: "card_yes"),
                                         "title" : "Оплата картой курьеру"],
                                        ["image" : #imageLiteral(resourceName: "Percent_red"),
                                         "title" : "Акции и скидки"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorColor = UIColor.clear
        tableView.separatorStyle = .none
        
        for _ in 0..<Parameters.count {
            SelectedCells.append(false)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Parameters.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterStoreParametersCell") as! FilterStoreParametersCell
        
        let parameter = self.Parameters[indexPath.row]
        
        cell.layer.cornerRadius = 20
        cell.layer.masksToBounds = true
        
        cell.cellImage.image = parameter["image"] as? UIImage
        cell.cellTitle.text = parameter["title"] as? String
        
        if self.SelectedCells[indexPath.row]{
            cell.checkBox.image = #imageLiteral(resourceName: "checkbox_active")
        } else {
            cell.checkBox.image = #imageLiteral(resourceName: "checkbox")
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath) as! FilterStoreParametersCell
        
        
        //get current status
        let status = SelectedCells[indexPath.row]
        
        if status {
            SelectedCells[indexPath.row] = false
            cell.checkBox.image = #imageLiteral(resourceName: "checkbox")
        }else{
            SelectedCells[indexPath.row] = true
            cell.checkBox.image = #imageLiteral(resourceName: "checkbox_active")

        }
        
        
        print(SelectedCells)
        print(delegate)
        
        delegate.SelectedStoreParameter(SelectedCells)
    }
}



