//
//  FilterStores.swift
//  Eda.Ac
//
//  Created by dingo on 29.11.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import DeckTransition
import Alamofire
import SwiftyJSON

struct storeCatsStruct {
    var id : String
    var title : String
    
    init() {
        id = ""
        title = ""
    }
}


class FilterStoresVC : UIViewController, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, FilterStoreParamatersDelegate {
    
    var delegate : FilterStoresDelegate!
    @IBOutlet weak var scroll_view: UIScrollView!

    var StoreParameters = [Bool]()
    
    var filterData = FilterData()
    
    //STORE CATS
    @IBOutlet weak var kitchensTableView: UITableView!
    @IBOutlet weak var kitchenTableViewHeight: NSLayoutConstraint!
    
    var StoreCats = [storeCatsStruct]()
    var SelectedCats = [String]()
    
    var filterEnabled = true
    
    @IBOutlet weak var applyFilterBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
    }
    
    func initView(){
        self.scroll_view.delegate = self
        
        self.kitchensTableView.dataSource = self
        self.kitchensTableView.delegate = self
        
        kitchensTableView.separatorStyle = .none
        kitchensTableView.separatorColor = UIColor.clear
        
        applyFilterBtn.layer.cornerRadius = applyFilterBtn.frame.height / 2
        applyFilterBtn.layer.masksToBounds = true
        applyFilterBtn.layer.shadowColor = UIColor.black.cgColor
        applyFilterBtn.layer.shadowOffset = CGSize(width: 5, height: 5)
        applyFilterBtn.layer.shadowRadius = 5
        applyFilterBtn.layer.shadowOpacity = 0.7
        
        
        getStoreCats()
    }
    
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        delegate.FilterStores(filterData, filterEnabled)
        filterEnabled = true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView.isEqual(scroll_view) else {
            return
        }
        
        if let delegate = transitioningDelegate as? DeckTransitioningDelegate {
            if scrollView.contentOffset.y > 0 {
                scrollView.bounces = true
                delegate.isDismissEnabled = false
            } else {
                if scrollView.isDecelerating {
                    view.transform = CGAffineTransform(translationX: 0, y: -scrollView.contentOffset.y)
                    scrollView.subviews.forEach {
                        $0.transform = CGAffineTransform(translationX: 0, y: scrollView.contentOffset.y)
                    }
                } else {
                    scrollView.bounces = false
                    delegate.isDismissEnabled = true
                }
            }
        }
    }
    
    func SelectedStoreParameter(_ cells: [Bool]) {
        let currentWorking = cells[0]
        let payType : PayType = cells[1] ? .Card : .AnyWay
        let Discount = cells[2]
        
        self.filterData.CurrentWorking = currentWorking
        self.filterData.PayType = payType
        self.filterData.Discounts = Discount
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "toFilterStoreParametersVC") {
            let filterParamsVC = segue.destination as! FilterStoreParametersVC
            
            let currentWorking = self.filterData.CurrentWorking
            let payType = self.filterData.PayType == .Card ? true : false
            let discount = self.filterData.Discounts
            
            filterParamsVC.SelectedCells = [currentWorking, payType, discount]
            filterParamsVC.delegate = self
        }
    }
    
    @IBAction func applyFilter(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func resetFilterValues(_ sender: Any) {
        //reset all objects
        self.filterData = FilterData()
        StoreCats.removeAll()
        SelectedCats.removeAll()
        StoreParameters.removeAll()

        //reset params vc
        let filterStoreParamVC = childViewControllers[0] as? FilterStoreParametersVC
        let currentWorking = self.filterData.CurrentWorking
        let payType = self.filterData.PayType == .Card ? true : false
        let discount = self.filterData.Discounts
        
        filterStoreParamVC?.SelectedCells = [currentWorking, payType, discount]
        filterStoreParamVC?.tableView.reloadData()
        
        initView()
        
        filterEnabled = false
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    //+++SELECT STORE CATS+++
    
    func getStoreCats(){
        self.StoreCats.removeAll()
        
        //TODO: show spinner
        let url = serverApiURL + "get-store-cats"
        
        Alamofire.request(url).responseJSON { response in
            if let result = response.result.value {
                let json = JSON(result)
                
                for index in 0..<json.arrayValue.count {
                    
                    let id = json[index]["id"].stringValue
                    let title = json[index]["title"].stringValue
                    
                    var store_cat = storeCatsStruct.init()

                    store_cat.id = id
                    store_cat.title = title
                    
                    self.StoreCats.append(store_cat)
                }
                
                let tableViewHeight = (self.StoreCats.count-1) * 50
                
                self.kitchenTableViewHeight.constant = CGFloat(tableViewHeight)
                self.kitchensTableView.reloadData()

            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return StoreCats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterSelectKitchenTableViewCell") as! FilterSelectKitchenTableViewCell
        
        cell.cellTitle.text = self.StoreCats[indexPath.row].title
        cell.checkBox.image = #imageLiteral(resourceName: "checkbox")
        
        cell.layer.cornerRadius = 20
        cell.layer.masksToBounds = true
        
        let storeId = self.StoreCats[indexPath.row].id

        for indexFilterData in 0..<filterData.Cats.count {
            if storeId == filterData.Cats[indexFilterData] {
                //Он уже выбран
                cell.checkBox.image = #imageLiteral(resourceName: "checkbox_active")
                self.SelectedCats.append(storeId)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let selectedCat = self.StoreCats[indexPath.row].id
        
        var isSelected = false
        var selectedIndex = Int()
        for index in 0..<SelectedCats.count {
            
            if selectedCat == SelectedCats[index] {
                //Он уже выбран
                isSelected = true
                selectedIndex = index
                break;
            }
        }
        
        let cell = tableView.cellForRow(at: indexPath) as! FilterSelectKitchenTableViewCell
        
        if isSelected {
            //Убрать с массива
            SelectedCats.remove(at: selectedIndex)
            
            cell.checkBox.image = #imageLiteral(resourceName: "checkbox")
        } else {
            SelectedCats.append(selectedCat)
            cell.checkBox.image = #imageLiteral(resourceName: "checkbox_active")
        }
        
        self.filterData.Cats = SelectedCats
    }
}
