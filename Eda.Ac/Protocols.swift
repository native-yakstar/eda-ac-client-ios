//
//  Protocols.swift
//  Eda.Ac
//
//  Created by dingo on 29.11.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import Foundation

protocol FilterStoreParamatersDelegate {
    func SelectedStoreParameter(_ cells : [Bool])
}

protocol FilterStoresDelegate {
    func FilterStores(_ filtereddata : FilterData, _ filterEnabled : Bool)
}

protocol FilterStoreCatsDelegate {
    func SelectedStoreCat(_ storesCats : [String])
}

protocol CheckOutClosed {
    func CheckOutClosed()
}
