//
//  CodeSnippets.swift
//  Eda.Ac
//
//  Created by dingo on 05.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

public func setShadow(view : UIView){
    view.layer.shadowColor = UIColor.black.cgColor
    view.layer.shadowOffset = CGSize(width: 1, height: 1)
    view.layer.shadowRadius = 5
    view.layer.shadowOpacity = 0.3
}

public func showAlert( _ title : String, _ view : UIViewController){
    let alert = UIAlertController(title : title , message : nil, preferredStyle : .alert)
    alert.addAction(UIAlertAction(title : "Закрыть", style : .default, handler : nil))
    view.present(alert, animated: true, completion: nil)
}

