//
//  SpinnerVC.swift
//  Eda.Ac
//
//  Created by dingo on 22.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import WebKit

class SpinnerVC : UIViewController {
    
    @IBOutlet weak var spinnerWebView: WKWebView!
    
    var timer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = Bundle.main.url(forResource: "spinner", withExtension: "html"){
            let request = URLRequest(url: url)
        
            spinnerWebView.load(request)
        }
        
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(presentMainVC), userInfo: nil, repeats: true)

        
    }

    @objc func presentMainVC(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainController = storyBoard.instantiateViewController(withIdentifier: "MainNavVC") as! MainNavVC
        self.present(mainController, animated: true, completion: nil)
    }
    
}
