//
//  StoreTableViewCell.swift
//  Eda.Ac
//
//  Created by dingo on 29.11.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit

class StoreTableViewCell: UITableViewCell {

    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var storeTitle: UILabel!
    @IBOutlet weak var storeCategory: UILabel!
    @IBOutlet weak var storepayTypeIndicator: UIImageView!
    @IBOutlet weak var storeMinPriceLabel: UILabel!
    @IBOutlet weak var storeDelieveType: UILabel!
    @IBOutlet weak var storeDelieveTime: UILabel!
    @IBOutlet weak var bgView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
