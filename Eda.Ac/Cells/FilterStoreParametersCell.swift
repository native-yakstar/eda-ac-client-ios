//
//  FilterStoreParametersCell.swift
//  Eda.Ac
//
//  Created by dingo on 30.11.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit

class FilterStoreParametersCell: UITableViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var checkBox: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

