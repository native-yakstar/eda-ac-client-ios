//
//  StoreProductsTableViewCell.swift
//  Eda.Ac
//
//  Created by dingo on 04.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit

class StoreProductsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productToBasketBtn: UIButton!
    @IBOutlet weak var productDesc: UILabel!
    @IBOutlet weak var productAppendBasketView: UIView!
    @IBOutlet weak var productPrependCount: UIButton!
    @IBOutlet weak var productAppendCount: UIButton!
    @IBOutlet weak var productInBasketCountLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        productToBasketBtn.layer.cornerRadius = productToBasketBtn.layer.frame.height / 2
        
        productAppendBasketView.layer.borderColor = mainColor.cgColor
        productAppendBasketView.layer.borderWidth = 1.0
        productAppendBasketView.layer.cornerRadius = productAppendBasketView.layer.frame.height / 2
        productAppendBasketView.layer.masksToBounds = true
        productAppendBasketView.isHidden = true
        
        setShadow(view: productToBasketBtn)
        setShadow(view: productAppendBasketView)
        setShadow(view: productImage)
        
        productImage.layer.cornerRadius = 20.0

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
