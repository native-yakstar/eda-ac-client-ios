//
//  PromotionCell.swift
//  Eda.Ac
//
//  Created by dingo on 05.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit

class PromotionCell: UITableViewCell {
    
    @IBOutlet weak var promotionImage: UIImageView!
    @IBOutlet weak var promotionTitle: UILabel!
    @IBOutlet weak var promotionDesc: UILabel!
    @IBOutlet weak var promotionToBasketBtn: UIButton!
    @IBOutlet weak var promotionPrice: UILabel!
    @IBOutlet weak var appendToBasketBtn: UIButton!
    @IBOutlet weak var prependFromBasketBtn: UIButton!
    @IBOutlet weak var promotionBasketCountLabel: UILabel!
    @IBOutlet weak var promotionManageBtnsView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        promotionToBasketBtn.layer.cornerRadius = promotionToBasketBtn.layer.frame.height / 2
        
        promotionManageBtnsView.layer.borderColor = mainColor.cgColor
        promotionManageBtnsView.layer.borderWidth = 1.0
        promotionManageBtnsView.layer.cornerRadius = promotionManageBtnsView.layer.frame.height / 2
        promotionManageBtnsView.layer.masksToBounds = true
        promotionManageBtnsView.isHidden = true
        
        setShadow(view: promotionToBasketBtn)
        setShadow(view: promotionManageBtnsView)
        setShadow(view: promotionImage)
        
        promotionImage.layer.cornerRadius = 20.0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
