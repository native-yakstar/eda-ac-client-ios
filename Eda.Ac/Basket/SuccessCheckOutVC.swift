//
//  SuccessCheckOutVC.swift
//  Eda.Ac
//
//  Created by dingo on 09.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit

class SuccessCheckOutVC : UIViewController {
    
    var OrderID = String()
    var StorePhone = String()
    
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var callBtn: UIButton!
    
    var delegate : CheckOutClosed!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        orderIdLabel.text = "Номер вашего заказа #\(OrderID)"
        callBtn.layer.cornerRadius = callBtn.layer.frame.height / 2
        setShadow(view: callBtn)
    }
    
    @IBAction func callAction(_ sender: Any) {
        if let url = URL(string: "tel://\(StorePhone)"){
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.delegate.CheckOutClosed()
        })
    }
    
}
