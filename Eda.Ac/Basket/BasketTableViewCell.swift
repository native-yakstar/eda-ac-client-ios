//
//  BasketTableViewCell.swift
//  Eda.Ac
//
//  Created by dingo on 05.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit

class BasketTableViewCell: UITableViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productDesc: UILabel!
    @IBOutlet weak var productCountLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productRemoveFromBasketBtn: UIButton!
    @IBOutlet weak var productPrependFromBasket: UIButton!
    @IBOutlet weak var productAppendToBasket: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        productCountLabel.layer.cornerRadius = productCountLabel.layer.frame.height / 4
        productCountLabel.layer.masksToBounds = true
        productCountLabel.layer.borderColor = UIColor.lightGray.cgColor
        productCountLabel.layer.borderWidth = 2.0
        
        productImage.layer.cornerRadius = 10.0
        productImage.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
