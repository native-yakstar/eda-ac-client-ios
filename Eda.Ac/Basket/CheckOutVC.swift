//
//  CheckOutVC.swift
//  Eda.Ac
//
//  Created by dingo on 07.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CheckOutVC : UIViewController, UITextFieldDelegate, CheckOutClosed {

    
    
    var products = [Product]()
    
    // UI : CONTROLLERS
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var streetField: UITextField!
    @IBOutlet weak var houseNumberField: UITextField!
    @IBOutlet weak var apartmentNumber: UITextField!
    @IBOutlet weak var entranceField: UITextField!
    @IBOutlet weak var floorField: UITextField!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var checkOutBtn: UIButton!

    
    //INFO VIEW
    @IBOutlet weak var infoViewTitle: UILabel!
    @IBOutlet weak var infoViewDescr: UILabel!
    @IBOutlet weak var infoViewCloseBtn: UIButton!
    @IBOutlet weak var infoView: UIView!
    
    
    // PROMOCODE VIEWS
    @IBOutlet weak var promocodeButton: UIButton!
    @IBOutlet weak var promocodeBackground: UIVisualEffectView!
    @IBOutlet weak var promocodeView: UIView!
    @IBOutlet weak var promocodeField: UITextField!
    @IBOutlet weak var promocodeActivateButton: UIButton!
    
    //SUCCESS PROMOCODE VIEW
    @IBOutlet weak var successPromocodeView: UIView!
    @IBOutlet weak var successPromocodeTitle: UILabel!
    @IBOutlet weak var successPromocodeName: UILabel!
    @IBOutlet weak var successPromocodeImage: UIImageView!
    @IBOutlet weak var successPromocodeCloseBtn: UIButton!
    
    // CARD PAY CONTROLLERS
    @IBOutlet weak var cardPayIcon: UIImageView!
    @IBOutlet weak var cardPayLabel: UILabel!
    @IBOutlet weak var cardPayActivateBtn: UIButton!
    
    //CASH PAY CONTROLLERS
    @IBOutlet weak var cashPayIcon: UIImageView!
    @IBOutlet weak var cashPayLabel: UILabel!
    @IBOutlet weak var cashPayActivateBtn: UIButton!
    
    
    var PAYTYPE : PayType = .Cash
    
    var BasketID = String()
    var BasketToken = String()
    
    var promocodeActivated = false
    var promocodeMeta = String()
    var promocodeID = String()
    
    var StorePhoneNumber = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        products = BasketHandler().getProductsInBasket()
        
        getStoreInfo()
        initView()
        getBasketInfo()
        
        
    }
    
    func getStoreInfo(){
        //Проверка если магазин принимает только по наличке

        var url = serverApiURL + "stores"
        
        url += "/\(self.products[0].storeId)"
        
        Alamofire.request(url).responseJSON { response in
            if let result = response.result.value {
                let json = JSON(result)
                
                self.StorePhoneNumber = json["phone"].stringValue
                
                let storePayType : PayType = json["cashless_payment"].stringValue == "0" ? .Cash : .Card
                
                //Если магазин поддерживает только наличную оплату
                if storePayType == .Cash {
                    //Деактивировать CARD PAY
                    self.cardPayIcon.image = #imageLiteral(resourceName: "card_no")
                    self.cardPayLabel.text = "Данный магазин не принимет оплату картой"
                    self.cardPayLabel.textColor = UIColor.darkGray
                    self.cardPayActivateBtn.setImage(#imageLiteral(resourceName: "checkbox"), for: .normal)
                    self.cardPayActivateBtn.isEnabled = false
    
                    //Выбрать CASH
                    self.selectPayType(payType: .Cash)
                } else {
                    //Выбрать CARD
                    self.selectPayType(payType: .Card)
                }
            
            }
        }
    }
    
    func getBasketInfo(){
        let url = serverApiURL + "cart/create-order"
        
        Alamofire.request(url).responseJSON { response in
            if let result = response.result.value {
                let json = JSON(result)
                
                self.BasketID = json["id"].stringValue
                self.BasketToken = json["token"].stringValue
                
            }
        }
    }
    
    func initView(){
        
        configureTextField(textField: phoneField)
        configureTextField(textField: nameField)
        configureTextField(textField: streetField)
        configureTextField(textField: houseNumberField)
        configureTextField(textField: apartmentNumber)
        configureTextField(textField: entranceField)
        configureTextField(textField: floorField)
        
        let userDefaults = UserDefaults.standard
        
        self.phoneField.text = userDefaults.string(forKey: "user_phone")
        self.nameField.text = userDefaults.string(forKey: "user_name")
        self.streetField.text = userDefaults.string(forKey: "user_street")
        self.houseNumberField.text = userDefaults.string(forKey: "user_house_number")
        
        self.checkOutBtn.layer.cornerRadius = self.checkOutBtn.frame.height / 2
        setShadow(view: checkOutBtn)
        
        self.promocodeButton.layer.cornerRadius = self.promocodeButton.frame.height / 2
        setShadow(view: promocodeButton)
        
        
        amountLabel.text = String(BasketHandler().getCurrentBasketTotalCount()) + " " + roubleSymbol
        
        
        //PROMOCODE VIEW
        promocodeBackground.alpha = 0.0
        promocodeView.layer.cornerRadius = 5.0
        setShadow(view: promocodeView)
        promocodeView.alpha = 0.0
        promocodeActivateButton.layer.cornerRadius = promocodeActivateButton.frame.height / 2
        setShadow(view: promocodeActivateButton)
        configureTextField(textField: promocodeField)
        
        //INFO VIEW
        infoView.layer.cornerRadius = 5.0
        setShadow(view: infoView)
        infoView.alpha = 0.0
        infoViewCloseBtn.layer.cornerRadius = infoViewCloseBtn.frame.height / 2
        setShadow(view: infoViewCloseBtn)
        
        //SUCCESS PROMOCODE VIEW
        successPromocodeView.layer.cornerRadius = 5.0
        setShadow(view: successPromocodeView)
        successPromocodeView.alpha = 0.0
        successPromocodeCloseBtn.layer.cornerRadius = successPromocodeCloseBtn.frame.height / 2
        setShadow(view: successPromocodeCloseBtn)
        successPromocodeImage.layer.cornerRadius = 10.0
        successPromocodeImage.layer.masksToBounds = true
        successPromocodeName.layer.cornerRadius = successPromocodeName.layer.frame.height / 2
        successPromocodeName.layer.masksToBounds = true
        

    }
    
    func selectPayType(payType : PayType){
        if payType == .Cash {
            //выбрать cash
            self.cashPayActivateBtn.setImage(#imageLiteral(resourceName: "checkbox_active"), for: .normal)
            self.cardPayActivateBtn.setImage(#imageLiteral(resourceName: "checkbox"), for: .normal)
            PAYTYPE = .Cash
        } else {
            //выбрать card
            self.cashPayActivateBtn.setImage(#imageLiteral(resourceName: "checkbox"), for: .normal)
            self.cardPayActivateBtn.setImage(#imageLiteral(resourceName: "checkbox_active"), for: .normal)
            PAYTYPE = .Card
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func configureTextField(textField : UITextField){
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x: 0.0, y: textField.frame.size.height - 1, width: textField.frame.size.width, height: 1.0)
        bottomBorder.backgroundColor = mainColor.cgColor
        textField.layer.addSublayer(bottomBorder)
        
        textField.delegate = self
        
        textField.clipsToBounds = true
        
    }
    
    @IBAction func cardPayBtnAction(_ sender: Any) {
        self.selectPayType(payType: .Card)
    }
    
    @IBAction func cashPayBtnAction(_ sender: Any) {
        self.selectPayType(payType: .Cash)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
    }
    
    @IBAction func checkOutAction(_ sender: Any) {
        //Валидация всех форм
        var allDataCorrect = true
        var neededFieldsList = [String]()
        
        if self.phoneField.text!.isEmpty {
            allDataCorrect = false
            neededFieldsList.append("Номер телефона")
        }
        
        if self.nameField.text!.isEmpty {
            allDataCorrect = false
            neededFieldsList.append("Имя")
        }
        
        if self.streetField.text!.isEmpty {
            allDataCorrect = false
            neededFieldsList.append("Улица")
        }
        
        if self.houseNumberField.text!.isEmpty {
            allDataCorrect = false
            neededFieldsList.append("Номер дома")
        }
        
        if !allDataCorrect {
            var notEnteredFields = String()
            for index in 0..<neededFieldsList.count {
                
                notEnteredFields += "\(neededFieldsList[index])"
                
                if index != notEnteredFields.count-1 {
                    notEnteredFields += "\n"
                }
            }
            
            infoViewTitle.text = "Не заполнены обязательные поля"
            infoViewTitle.textColor = redColor
            infoViewDescr.text = notEnteredFields
            infoViewCloseBtn.setTitle("Закрыть", for: .normal)
            
            showPopupView(popupView: infoView)
            
        } else {
            self.UploadBasketToServer()
        }
    }
    
    func UploadBasketToServer(){
        let phoneNumber = self.phoneField.text!
        let name = self.nameField.text!
        var address = "улица \(String(describing: self.streetField.text!)) \(String(describing: self.houseNumberField.text!)), кв \(String(describing: self.apartmentNumber.text!))"
        
        if !self.entranceField.text!.isEmpty {
            address += ", подъезд : \(String(describing: self.entranceField.text!))"
        }
        
        if !self.floorField.text!.isEmpty {
            address += ", этаж :\(String(describing: self.floorField.text!))"
        }
        
        
        
        
        //reformat basket
        
        var productsJSON = [JSON]()
        
        let products = BasketHandler().getProductsInBasket()
        
        for product in products {
            
            var jsonProduct = JSON()
            
            jsonProduct["p_id"].string = product.id
            jsonProduct["amount"].string = String(product.count)
            jsonProduct["s_id"].string = product.storeId
            jsonProduct["type"].string = product.type == .Product ? "0" : "1"
            
            
            productsJSON.append(jsonProduct)
            
        }
        
        var ResponceJSON = JSON().dictionaryObject

        ResponceJSON = ["customer_name" : name,
                        "customer_address" : address,
                        "customer_phone" : phoneNumber,
                        "products" : productsJSON,
                        "id" : BasketID,
                        "token" : BasketToken,
                        "payment_type" : self.PAYTYPE == .Cash ? "0" : "1",
                        "promocode_id" : self.promocodeActivated ? self.promocodeID : String()]
        
        print(JSON(ResponceJSON!))
        
        
        //Upload
        let url = URL(string: serverApiURL + "checkout")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = try! JSON(ResponceJSON!).rawData()
        
        Alamofire.request(request).responseJSON { response in
            
            if let result = response.result.value {
                let json = JSON(result)
                
                if json["success"].boolValue {
                    //save data
                    let userDefaults = UserDefaults.standard
                    
                    userDefaults.set(self.phoneField.text, forKey: "user_phone")
                    userDefaults.set(self.nameField.text, forKey: "user_name")
                    userDefaults.set(self.streetField.text, forKey: "user_street")
                    userDefaults.set(self.houseNumberField.text, forKey: "user_house_number")
                    userDefaults.synchronize()
                    
                    //Clear basket
                    
                    BasketHandler().clearBasket()
                    
                    //navigate to success VC
                    //SuccessCheckOutVC
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "BasketView", bundle: nil)
                    let successCheckOutVC = storyBoard.instantiateViewController(withIdentifier: "SuccessCheckOutVC") as! SuccessCheckOutVC
                    
                    successCheckOutVC.OrderID = self.BasketID
                    successCheckOutVC.StorePhone = self.StorePhoneNumber
                    successCheckOutVC.delegate = self
                    
                    
                    let navController = UINavigationController(rootViewController: successCheckOutVC)
                    self.present(navController, animated: true, completion: nil)
                } else {
                    let alert = UIAlertController(title : "Ошибка при оформлении", message : json["msg"].stringValue + json["data"].stringValue, preferredStyle : .alert)
                    
                    alert.addAction(UIAlertAction(title : "Закрыть", style : .cancel, handler : nil))
                    
                    self.present(alert, animated: true, completion: nil)
                }

                print(json)
                
            }
        }
        
    }
    
    @IBAction func showPromocodeView(_ sender: Any) {
        if self.promocodeActivated {
            showInfoView("ПРОМОКОД АКТИВИРОВАН", self.promocodeMeta, mainColor)
        } else {
            showPopupView(popupView: promocodeView)
        }
    }
    
    // POPUP

    func showPopupView (popupView : UIView) {
        
        UIView.transition(with: popupView,
                          duration: 0.3,
                          options: .transitionFlipFromTop,
                          animations: {
                            popupView.alpha = 1.0
                            self.promocodeBackground.alpha = 1.0
        })

    }
    
    
    func hidePopupView (popupView : UIView){

        UIView.transition(with: popupView,
                          duration: 0.3,
                          options: .transitionFlipFromBottom,
                          animations: {
                            popupView.alpha = 0.0
        })
        UIView.transition(with: self.promocodeBackground,
                          duration: 0.4,
                          options: .transitionFlipFromBottom,
                          animations: {
                            self.promocodeBackground.alpha = 0.0
        })
        
    }
    
    //PROMOCODE
    
    @IBAction func activatePromocodeButtonAction(_ sender: Any) {
        self.hidePopupView(popupView: promocodeView)
        self.validatingPromocode(promocode: self.promocodeField.text!)
    }
    
    //INFO

    @IBAction func closeInfoViewBtnAction(_ sender: Any) {
        self.hidePopupView(popupView: infoView)
    }
    
    //SUCCESS PROMOCODE
    
    @IBAction func successPromocodeBtnAction(_ sender: Any) {
        self.hidePopupView(popupView: successPromocodeView)
        
        //Send server promocode
        
    }
    
    
    func validatingPromocode(promocode : String) {
        
        let url = serverApiURL + "checkout/check-promocode"
        
        let parameters: Parameters = ["code": promocode]

        
        Alamofire.request(url, method : .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in
            if let result = response.result.value {
                let responce = JSON(result)
                
                
                print(responce)
                
                if !responce["success"].boolValue {
                    //Промокод недействительный
                    self.showInfoView("Недействительный промокод", "Пожалуйста, проверьте правильно ли набран промокод", redColor)
                } else {
                    //Там чето есть
                    
                    let json = responce["msg"]
                    
                    let p_type = self.identifyDiscount(id: json["type"].stringValue)
                    var promocode = PROMOCODE(p_type)
                    
                    promocode.title = json["type_title"].stringValue
                    promocode.name = json["p_title"].stringValue
                    promocode.id = json["id"].stringValue
                    
                    
                    if !json["percent"].stringValue.isEmpty {
                        promocode.percent = json["percent"].intValue
                        promocode.percent_promocode = true
                    }
                    if !json["discount"].stringValue.isEmpty {
                        promocode.discount = json["discount"].intValue
                        promocode.percent_promocode = false
                    }
                    if !json["p_id"].stringValue.isEmpty {
                        promocode.p_id = json["p_id"].stringValue
                    }
                    
                    self.initPromocode(promocode)
                }
                
            }
        }
    }
    
    func initPromocode(_ promocode : PROMOCODE){
        switch promocode.type {
            case .productInGift:
                getProductBy(Promocode: promocode)
                break
            case .DiscountForTotalCount:
                showDiscountForTotalCount(promocode: promocode)
                break
            case .DiscountForSpecificProduct:
                showDiscountForSpecificProduct(promocode: promocode)
                break
            case .Unknown:
                break
        }
    }
    
    func getProductBy(Promocode : PROMOCODE) {
        var url = serverApiURL + "products"
        
        let currentStoreId = BasketHandler().getCurrentStoreId()
        
        url += "/\(Promocode.p_id)?s_id=\(currentStoreId)"
        
        Alamofire.request(url).responseJSON { response in
            if let result = response.result.value {
                let json = JSON(result)
                
                
                let imageURL = json["image"].stringValue
                let title = json["title"].stringValue
                
                self.showProductGiftView(Promocode.title, title, imageURL, Promocode)
                
            }
        }
    }
    
    func showDiscountForTotalCount(promocode : PROMOCODE){
        if promocode.percent_promocode {
            //Вычитать с общей суммы корзины
            var totalCount = BasketHandler().getCurrentBasketTotalCount()
            
            let minusCount = (totalCount / 100) * Float(promocode.percent)
            
            totalCount -= minusCount
            self.amountLabel.text = "\(totalCount) \(roubleSymbol)"
            
            
            self.showInfoView(promocode.title, "Активирована скидка в \(promocode.percent) процентов", mainColor)
            
            self.ACTIVATEPROMOCODE(promocode: promocode, meta: "Вы активировали скидку в \(promocode.percent) процентов")

        } else {
            var totalCount = BasketHandler().getCurrentBasketTotalCount()
            
            totalCount = totalCount - Float(promocode.discount)
            self.amountLabel.text = "\(totalCount) \(roubleSymbol)"
            
            
            self.showInfoView(promocode.title, "Активирована скидка на \(promocode.discount) \(roubleSymbol)", mainColor)
            
            self.ACTIVATEPROMOCODE(promocode: promocode, meta: "Вы активировали скидку на \(promocode.discount) \(roubleSymbol)")
        }
    }
    
    func showDiscountForSpecificProduct(promocode : PROMOCODE){
        //Определить есть ли этот товар в корзине
        if BasketHandler().productAvailableInBasket(product_id: promocode.p_id){
            
            var url = serverApiURL + "products"
            
            let currentStoreId = BasketHandler().getCurrentStoreId()
            let product_count_in_basket = BasketHandler().productCountInBasket(product_id: promocode.p_id)
            
            url += "/\(promocode.p_id)?s_id=\(currentStoreId)"
            
            Alamofire.request(url).responseJSON { response in
                if let result = response.result.value {
                    let json = JSON(result)
                    
                    
                    let imageURL = json["image"].stringValue
                    let product_title = json["title"].stringValue
                    let price = json["price"].floatValue
                    
                    var totalCount = BasketHandler().getCurrentBasketTotalCount()

                    if promocode.percent_promocode {
                        
                        totalCount -= ((price / 100) * Float(promocode.percent) * Float(product_count_in_basket))
                        
                        self.amountLabel.text = "\(totalCount) \(roubleSymbol)"
                        
                        self.showProductGiftView("Активирована скидка на \(promocode.percent) процентов", product_title, imageURL, promocode)
                        
                        self.ACTIVATEPROMOCODE(promocode: promocode, meta: "Вы активировали скидку на \(promocode.percent) процентов для \(product_title)")
                    } else {
                        var totalCount = BasketHandler().getCurrentBasketTotalCount()
                        
                        totalCount = totalCount - (price - Float(promocode.discount))
                        self.amountLabel.text = "\(totalCount) \(roubleSymbol)"
                        
                        self.showProductGiftView("Активирована скидка на \(promocode.discount) \(roubleSymbol)", product_title, imageURL, promocode)
                        
                        self.ACTIVATEPROMOCODE(promocode: promocode, meta: "Вы активировали скидку на \(promocode.discount) \(roubleSymbol) для \(product_title)")
                    }
                }
            }
            
            
        } else {
            self.showInfoView("Внимание", "Товар, на который вы хотите приобрести скидку отсутсвует в корзине", mainColor)
        }
    }
    
    func showProductGiftView(_ promocodeName : String, _ productName : String, _ productImageURL : String, _ promocode : PROMOCODE){
        if let url = URL(string: serverImageUrl + productImageURL){
            self.successPromocodeImage.af_setImage(withURL: url)
        }
        self.successPromocodeName.text = productName
        self.successPromocodeTitle.text = promocodeName
        self.showPopupView(popupView: successPromocodeView)
        
        self.ACTIVATEPROMOCODE(promocode: promocode, meta: "Вы получили в подарок \(productName)")
    }
    
    func showInfoView(_ title : String, _ message : String, _ titleColor : UIColor){
        self.infoViewTitle.text = title
        self.infoViewDescr.text = message
        self.infoViewTitle.textColor = titleColor
        self.showPopupView(popupView: self.infoView)
    }
    
    
    func identifyDiscount(id : String) -> promocodeType {
        
        switch id {
        case "1":
            return .productInGift
        case "2":
            return .DiscountForTotalCount
        case "3":
            return .DiscountForSpecificProduct
        default:
            return .Unknown
        }
    }
    
    
    // DISABLE DISCOUNT BUTTON AND SEND PROMOCODE TO SERVER
    
    func ACTIVATEPROMOCODE(promocode : PROMOCODE, meta : String){
        //disable promocode btn
        self.promocodeButton.setTitle("ПРОМОКОД АКТИВИРОВАН", for: .normal)
        self.promocodeActivated = true
        self.promocodeID = promocode.id
        self.promocodeMeta = meta
    }
    
    
    func CheckOutClosed() {
        self.dismiss(animated: true, completion: nil)
    }
}

struct PROMOCODE {
    var id : String
    var title : String
    var name : String
    var percent : Int
    var discount : Int
    var code : String
    var type : promocodeType
    var typeString : String
    var percent_promocode : Bool
    var p_id : String
    var product_title : String
    var statusActive : Bool
    
    init(_ promocode_type : promocodeType) {
        id = String()
        title = String()
        name = String()
        percent = 0
        discount = 0
        code = String()
        type = promocode_type
        typeString = "1"
        p_id = String()
        product_title = String()
        percent_promocode = true
        statusActive = true
    }
}

enum promocodeType {
    case productInGift
    case DiscountForTotalCount
    case DiscountForSpecificProduct
    case Unknown
}
