//
//  BasketVC.swift
//  Eda.Ac
//
//  Created by dingo on 05.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import AlamofireImage

class BasketVC : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var products = [Product]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var checkOutBtn: UIButton!
    var totalCountLabel = UILabel()
    @IBOutlet weak var nullBasketView: UIView!
    @IBOutlet weak var closeViewBtn: UIButton!
    @IBOutlet weak var minPriceNotReachedLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: mainColor]
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "arrow"), style: .plain, target: self, action: #selector(hideVC))
        navigationController?.navigationBar.tintColor = mainColor
        title = "Корзина"
        
        tableView.delegate = self
        tableView.dataSource = self
        
        
        checkOutBtn.layer.cornerRadius = checkOutBtn.layer.frame.height / 2
        checkOutBtn.layer.masksToBounds = true
        
        closeViewBtn.layer.cornerRadius = closeViewBtn.layer.frame.height / 2
        closeViewBtn.layer.masksToBounds = true
        closeViewBtn.addTarget(self, action: #selector(self.hideVC), for: .touchUpInside)
        
        totalCountLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 80, height: 25))
        totalCountLabel.backgroundColor = mainColor
        totalCountLabel.layer.cornerRadius = totalCountLabel.frame.height / 2
        totalCountLabel.layer.masksToBounds = true
        totalCountLabel.textAlignment = .center
        totalCountLabel.textColor = UIColor.white
        
        let totalCountItem = UIBarButtonItem(customView: totalCountLabel)
        
        self.navigationItem.rightBarButtonItem = totalCountItem
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.reloadUI), name: .basketChanged, object: nil)
        
        initView()
    }
    
    func initView(){
        products = BasketHandler().getProductsInBasket()
        
        UIView.animate(withDuration: 0.3){
            self.nullBasketView.isHidden = self.products.count != 0
        }
        
        var totalCount : Float = 0
        for product in products {
            totalCount += (product.price) * Float(product.count)
        }
        
        self.totalCountLabel.text = String(totalCount) + " " + roubleSymbol
        self.checkOutBtn.setTitle("ОФОРМИТЬ \(totalCount) \(roubleSymbol)", for: .normal)
        
        let minPriceDifference = Float(BasketHandler().getCurrentStoreMinPrice()) - BasketHandler().getCurrentBasketTotalCount()
        
        if minPriceDifference > 0 {
            
            self.minPriceNotReachedLabel.isHidden = false
            self.minPriceNotReachedLabel.text = "Не хватает \(minPriceDifference) \(roubleSymbol) до минимальной суммы заказа"
            setShadow(view: self.minPriceNotReachedLabel)
            self.checkOutBtn.isEnabled = false
        } else {
            self.minPriceNotReachedLabel.isHidden = true
            setShadow(view: self.checkOutBtn)
            self.checkOutBtn.isEnabled = true
        }
        
        self.tableView.reloadData()
    }
    
    @objc func hideVC(){
        self.dismiss(animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasketTableViewCell") as! BasketTableViewCell
        
        let product = self.products[indexPath.row]
        
        if let url = URL(string: serverImageUrl + product.imageURL){
            cell.productImage.af_setImage(withURL: url)
        }
        
        cell.productTitle.text = product.title
        cell.productDesc.text = product.desc
        cell.productCountLabel.text = "\(product.count)"
        cell.productPriceLabel.text = "\(product.price * Float(product.count)) \(roubleSymbol)"
        
        
        cell.productAppendToBasket.tag = indexPath.row
        cell.productPrependFromBasket.tag = indexPath.row
        cell.productRemoveFromBasketBtn.tag = indexPath.row
        
        cell.productAppendToBasket.addTarget(self, action: #selector(AppendToBasket(_:)), for: .touchUpInside)
        cell.productPrependFromBasket.addTarget(self, action: #selector(PrependBasket(_:)), for: .touchUpInside)
        cell.productRemoveFromBasketBtn.addTarget(self, action: #selector(RemoveProductBasket(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if self.products[indexPath.row].type == .Promotion {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Products", bundle: nil)
            let promotionController = storyBoard.instantiateViewController(withIdentifier: "ChoosePromotionVC") as! ChoosePromotionVC
            
            
            promotionController.promotion = self.products[indexPath.row]
            
            self.navigationController?.pushViewController(promotionController, animated: true)
            
//            let navController = UINavigationController(rootViewController: promotionController)
//            self.present(navController, animated: true, completion: nil)
        } else {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Products", bundle: nil)
            let productController = storyBoard.instantiateViewController(withIdentifier: "ChooseProductVC") as! ChooseProductVC
            productController.product = self.products[indexPath.row]
            
            self.navigationController?.pushViewController(productController, animated: true)

            //self.navigationController?.popToViewController(productController, animated: true)
            //let navController = UINavigationController(rootViewController: productController)
            //self.present(navController, animated: true, completion: nil)
        }
    }
    
    @objc func AppendToBasket(_ sender : UIButton) {
        if BasketHandler().handleBasket(handle: .append, current_product: self.products[sender.tag]){
            reloadUI()
        }
    }
    
    @objc func PrependBasket(_ sender : UIButton) {
        if BasketHandler().handleBasket(handle: .prepend, current_product: self.products[sender.tag]){
            reloadUI()
        }
    }
    
    @objc func RemoveProductBasket(_ sender : UIButton) {
        if BasketHandler().handleBasket(handle: .remove, current_product: self.products[sender.tag]){
            reloadUI()
        }
    }
    
    @objc func reloadUI(){
        UIView.transition(with: tableView,
                          duration: 0.15,
                          options: .transitionCrossDissolve,
                          animations: { self.initView() })
    }
    
    @IBAction func toCheckOut(_ sender: Any) {
        
    }
}
