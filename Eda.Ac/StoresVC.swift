//
//  StoresVC.swift
//  Eda.Ac
//
//  Created by dingo on 22.11.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireImage
import DTZFloatingActionButton
import DeckTransition
import YTBarButtonItemWithBadge


struct FilterData {
    var Cats : [String]
    var CurrentWorking : Bool
    var PayType : PayType
    var Discounts : Bool
    
    init() {
        Cats = [String]()
        CurrentWorking = false
        PayType = .AnyWay
        Discounts = false
    }
}

enum PayType {
    case Cash
    case Card
    case AnyWay
}

class StoresVC : UITableViewController, FilterStoresDelegate, UIGestureRecognizerDelegate {
    
    var Stores = [StoreInfo]()
    
    var filterData = FilterData()
    
    var FilterEnabled = false
    
    let basketBtn = YTBarButtonItemWithBadge();

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorColor = UIColor.clear
        tableView.separatorStyle = .none
        
        tableView.backgroundColor = UIColor.clear
        
        let bgView = UIImageView(frame: tableView.bounds)
        bgView.image = UIImage(named: "bg")
        tableView.backgroundView = bgView

        
        loadStores(FilterEnabled, filterData)
        
        //NAV BAR
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: mainColor]
        
        //FAB

        let actionButton = DTZFloatingActionButton()
        actionButton.handler = {
            button in
            self.showFilter()
        }
        actionButton.isScrollView = true
        actionButton.buttonImage = #imageLiteral(resourceName: "filtr")
        actionButton.buttonColor = mainColor
        
        self.view.addSubview(actionButton)
        
        
        //Create basket badge
        basketBtn.setHandler(callback: openBasket);
        basketBtn.setImage(image: #imageLiteral(resourceName: "backet"));
        basketBtn.setBadge(value: BasketHandler().getCurrentBasketTotalCount());
        self.navigationItem.setRightBarButton(basketBtn.getBarButtonItem(), animated: true);
        
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.updateBasketUI), name: .basketChanged, object: nil)
        
        
        self.basketBtn.setBadge(value: BasketHandler().getCurrentBasketTotalCount());
        
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        lpgr.minimumPressDuration = 5.0 as CFTimeInterval
        lpgr.delegate = self
        tableView.addGestureRecognizer(lpgr)
    }
    
    func showFilter(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Filters", bundle: nil)
        let selectCategory = storyBoard.instantiateViewController(withIdentifier: "FilterStoresVC") as! FilterStoresVC
        selectCategory.delegate = self
        
        selectCategory.filterData = self.filterData
        
        let transitionDelegate = DeckTransitioningDelegate()
        selectCategory.transitioningDelegate = transitionDelegate
        selectCategory.modalPresentationStyle = .custom
        present(selectCategory, animated: true, completion: nil)
    }
    
    func FilterStores(_ filtereddata: FilterData, _ filterEnabled : Bool) {
        self.FilterEnabled = filterEnabled
        self.filterData = filtereddata
        
        
        loadStores(filterEnabled, filterData)
        print(filterEnabled)
        print(filtereddata)
    }
    
    
    
    func loadStores(_ filterEnabled : Bool, _ filterData : FilterData){
        
        
        var url = serverApiURL + "stores"
        
        print(filterEnabled)
        print(filterData)
        if filterEnabled {
            //Задаю параметры запроса
            url += "?"
            
            //Если есть категории то добавляю в cat_id и разделяю каждый через ","
            if filterData.Cats.count != 0 {
                url += "cat_id="
                var secondCatFlag = false
                for cat in filterData.Cats {
                    if secondCatFlag {
                        url += ","
                    }else{
                        secondCatFlag = true
                    }
                    url += "\(cat)"
                }
            }
            
            //Если флаг сейчас работающих истинный то добавляю его в запрос
            if filterData.CurrentWorking { url += "&storeOnline=1" }
            
            //Если тип оплаты НЕ AnyWay то добавляю в запрос
            if filterData.PayType != .AnyWay {
                if filterData.PayType == .Cash {
                    //Если тип оплаты только наличка
                    url += "&cashlessPayment=2"
                } else {
                    //Если тип оплаты только картой
                    url += "&cashlessPayment=1"
                }
            }
        }
        
        print(url)
        
        Alamofire.request(url).responseJSON { response in

            if let result = response.result.value {
                let json = JSON(result)
                
                self.Stores.removeAll()
                
                for index in 0..<json.arrayValue.count{
                    let store = json[index]
                    
                    
                    ///PARSE MAIN DATA
                    let id : String = store["id"].stringValue
                    let title : String = store["title"].stringValue
                    let desc : String = store["desc"].stringValue
                    let background : String = store["background"].stringValue
                    let delivery : String = store["delivery"].stringValue
                    let image : String = store["image"].stringValue
                    //remove seconds
                    var _hours_from = store["hours_from"].stringValue
                    _hours_from = _hours_from.components(separatedBy: ":")[0] + ":" + _hours_from.components(separatedBy: ":")[1]
                    
                    var _hours_to = store["hours_from"].stringValue
                    _hours_to = _hours_to.components(separatedBy: ":")[0] + ":" +
                        _hours_to.components(separatedBy: ":")[1]
                    
                    let hours_from : String = _hours_from
                    let hours_to : String = _hours_to
                    let address : String = store["address"].stringValue
                    let payType : PayType = store["cashless_payment"].stringValue == "0" ? .Cash : .AnyWay
                    let lat : String = store["lat"].stringValue
                    let lon : String = store["lon"].stringValue
                    let min_price_order : Int = store["min_price_order"].intValue
                    let delivery_time : String = store["delivery_time"].stringValue
                    
                    //PARSE CATEGORIES
                    var categories = [storeCatsStruct]()
                
                    for cats_index in 0..<store["categories"].arrayValue.count {
                        var cat = storeCatsStruct.init()
                        
                        cat.id = store["categories"][cats_index]["id"].stringValue
                        cat.title = store["categories"][cats_index]["title"].stringValue
                        
                        categories.append(cat)
                    }
                    
                    
                    //CREATE SUPER STORE VARIABLE
                    var STORE = StoreInfo()
                    STORE.id = id
                    STORE.title = title
                    STORE.desc = desc
                    STORE.background = background
                    STORE.delivery = delivery
                    STORE.image = image
                    STORE.hours_from = hours_from
                    STORE.hours_to = hours_to
                    STORE.address = address
                    STORE.payType = payType
                    STORE.lat = lat
                    STORE.lon = lon
                    STORE.min_price_order = min_price_order
                    STORE.delivery_time = delivery_time

                    
                    self.Stores.append(STORE)
                }
                
                self.tableView.reloadData()
            }
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StoreTableViewCell") as! StoreTableViewCell
        
        let store = self.Stores[indexPath.row]
        
        
        if let url = URL(string: serverImageUrl + store.image) {
            cell.storeImage.af_setImage(withURL: url)
        } else {
            //set default image
            
        }
        
        cell.storeImage.layer.cornerRadius = 10.0
        cell.storeImage.layer.masksToBounds = true
        
        
        cell.storeTitle.text = store.title
        var Categories = String()
        
        for category in store.category {
            Categories += category.title + " / "
        }
        
        //Удаление последнего символа
        Categories = String(Categories.dropLast().dropLast())
        
        cell.storeCategory.text = Categories
        
        if store.payType == .Cash {
            cell.storepayTypeIndicator.image = #imageLiteral(resourceName: "card_no")
        } else {
            cell.storepayTypeIndicator.image = #imageLiteral(resourceName: "card_yes")
        }
        
        cell.storeMinPriceLabel.text = String(store.min_price_order) + " " + roubleSymbol
        cell.storeDelieveType.text = store.delivery
        
        cell.storeDelieveTime.text = store.delivery_time + " мин."
        
        //background
        cell.backgroundView?.backgroundColor = UIColor.clear
        cell.backgroundColor = UIColor.clear
        
        
        cell.bgView.layer.shadowColor = UIColor.black.cgColor
        cell.bgView.layer.shadowOffset = CGSize(width: 1, height: 1)
        cell.bgView.layer.shadowRadius = 5
        cell.bgView.layer.shadowOpacity = 0.3
        cell.bgView.layer.cornerRadius = 10.0
        //cell.bgView.layer.masksToBounds = true
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.clear
        cell.selectedBackgroundView = bgColorView
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "toStoreVC") {
            let storeVC = segue.destination as! StoreVC
            
            if let indexPath = self.tableView.indexPathForSelectedRow {
                storeVC.Store = self.Stores[indexPath.row]
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Stores.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.last == indexPath.row {
            //Open AdminVC
        }
        
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @objc func updateBasketUI(){
        UIView.transition(with: view,
                          duration: 0.15,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.basketBtn.setBadge(value: BasketHandler().getCurrentBasketTotalCount()) })
    }
    
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer) {
        let p: CGPoint = gestureRecognizer.location(in: tableView)
        let indexPath: IndexPath? = tableView.indexPathForRow(at: p)
        if indexPath != nil {
            if indexPath?.last == indexPath?.row {
                print("OPEN ADMIN PANEL")
                let storyBoard: UIStoryboard = UIStoryboard(name: "Admin", bundle: nil)
                let productController = storyBoard.instantiateViewController(withIdentifier: "AdminLoginVC") as! AdminLoginVC
                
                let navController = UINavigationController(rootViewController: productController)
                self.present(navController, animated: true, completion: nil)
            }
        }
        
    }

    
    func openBasket(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "BasketView", bundle: nil)
        let productController = storyBoard.instantiateViewController(withIdentifier: "BasketVC") as! BasketVC
        
        let navController = UINavigationController(rootViewController: productController)
        self.present(navController, animated: true, completion: nil)
    }
    
}
