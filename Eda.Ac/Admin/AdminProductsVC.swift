//
//  AdminChooseProductsVC.swift
//  Eda.Ac
//
//  Created by dingo on 11.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
import SwiftSpinner


struct adminFullProduct {
    var id : String
    var desc : String
    var composition : String
    var image : String
    var weight : String
    var price : Float
    var title : String
    var category_title : String
    var category_id : String
    var s_id : String
    var type : ProductType
    var amount : Int

    
    init() {
        id = String()
        desc = String()
        composition = String()
        image = String()
        weight = String()
        price = 0.0
        title = String()
        category_title = String()
        category_id = String()
        s_id = String()
        type = .Product
        amount = 0
    }
}

class AdminProductsVC : UITableViewController {
    
    var Products = [adminFullProduct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Товары"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Создать", style: .plain, target: self, action: #selector(createProduct))

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getProducts()
    }
    
    @objc func createProduct(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Admin", bundle: nil)
        
        let chooseProductController = storyBoard.instantiateViewController(withIdentifier: "AdminChooseProductVC") as! AdminChooseProductVC
        
        chooseProductController.PRODUCT = adminFullProduct()
        chooseProductController.createProduct = true

        self.navigationController?.pushViewController(chooseProductController, animated: true)
    }
    
    func getProducts(){
        SwiftSpinner.show("Загрузка")
        
        API().adminFetchProducts(completion : { products in
            self.Products = products
            
            self.tableView.reloadData()
            
            SwiftSpinner.hide()
        })
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdminProductsTableViewCell") as! AdminProductsTableViewCell
        
        let product = self.Products[indexPath.row]
        
        if let url = URL(string : serverImageUrl + product.image) {
            cell.productImage.af_setImage(withURL: url)
        }
        
        cell.productDesc.text = product.desc
        cell.productPrice.text = "\(product.price) \(roubleSymbol)"
        cell.productTitle.text = product.title
        cell.productWeight.text = "\(product.weight) гр."
        cell.productCategory.text = product.category_title
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Products.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Admin", bundle: nil)
        
        let chooseProductController = storyBoard.instantiateViewController(withIdentifier: "AdminChooseProductVC") as! AdminChooseProductVC

        chooseProductController.PRODUCT = self.Products[indexPath.row]
        
        self.navigationController?.pushViewController(chooseProductController, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let optionsAction = UIContextualAction(style: .normal, title:  "Удалить", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.removeProduct(index: indexPath.row)
            success(true)
        })
        optionsAction.backgroundColor = UIColor.red
        
        return UISwipeActionsConfiguration(actions: [optionsAction])
    }
    
    func removeProduct(index : Int){
        let alert = UIAlertController(title : "Удалить продукт?", message : nil, preferredStyle : .alert)
        alert.addAction(UIAlertAction(title : "Удалить", style : .destructive, handler : { (UIAlertAction) -> Void in
            API().adminRemoveProduct(product: self.Products[index], completion : { result in
                showAlert(result["msg"].stringValue, self)
            })
        }))
        alert.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil ))
        
        self.present(alert, animated: true, completion: nil)
    }
}
