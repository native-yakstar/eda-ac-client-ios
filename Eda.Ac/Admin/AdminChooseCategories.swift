//
//  AdminChooseCategories.swift
//  Eda.Ac
//
//  Created by dingo on 12.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSpinner
import SwiftyJSON

protocol SelectCategoryDelegate {
    func selectedCat(cat : category)
}

class AdminChooseCategories: UITableViewController {
    
    var cats = [category]()
    
    var delegate : SelectCategoryDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getCats()
    }
    
    func getCats(){
        SwiftSpinner.show("Загрузка")
        var url = serverApiURL + "panel/products-categories?limit=1000&page=0"
        
        url += "&access-token=\(adminAccessToken)"
        
        print(url)
        
        
        Alamofire.request(url).responseJSON { response in
            if let result = response.result.value {
                let json = JSON(result)
                
                self.cats.removeAll()
                
                for cat in json.arrayValue {
                    
                    let id = cat["id"].stringValue
                    let title = cat["title"].stringValue
                    
                    var _cat = category()
                    
                    _cat.id = id
                    _cat.title = title
                    
                    self.cats.append(_cat)
                }
                
                self.tableView.reloadData()
                SwiftSpinner.hide()
            }else {
                SwiftSpinner.hide()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdminChooseCategoriesCell")
        
        cell?.textLabel?.text = self.cats[indexPath.row].title
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cats.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.selectedCat(cat: self.cats[indexPath.row])

        self.navigationController?.popViewController(animated: true)
    }
}
