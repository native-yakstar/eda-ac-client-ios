//
//  AdminChoosePromotionsVC.swift
//  Eda.Ac
//
//  Created by dingo on 13.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import SwiftyJSON
import SwiftSpinner

class AdminChoosePromotionVC : UITableViewController, UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UIPopoverControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var priceField: UITextField!
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var descTextView: UITextView!
    
    var picker : UIImagePickerController? = UIImagePickerController()

    let TITLE_FIELD_TAG_ID = 0
    let PRICE_FIELD_TAG_ID = 1
    let DESC_TEXT_VIEW_TAG_ID = 2
    
    
    var PROMOTION = adminFullPromotion()
    
    var createPromotion = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Акция"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: createPromotion ? "Опубликовать" : "Сохр.", style: .plain, target: self, action: #selector(savePromotion))
        
        picker?.delegate = self
        
        titleField.text = PROMOTION.title
        priceField.text = String(PROMOTION.price)
        statusButton.setTitle(PROMOTION.status == "1" ? "Включен" : "Выключен", for: .normal)
        statusButton.addTarget(self, action: #selector(self.changeStatus), for: .touchUpInside)
        
        if let url = URL(string : serverImageUrl + PROMOTION.image){
            imageView.af_setImage(withURL: url)
        }
        imageButton.setTitle(createPromotion ? "Добавить" : "Изменить", for: .normal)
        imageButton.addTarget(self, action: #selector(self.productPhotoUpload), for: .touchUpInside)
        descTextView.text = PROMOTION.desc
        
        titleField.delegate = self
        titleField.tag = TITLE_FIELD_TAG_ID
        
        priceField.delegate = self
        priceField.tag = PRICE_FIELD_TAG_ID
        
        descTextView.delegate = self
        descTextView.tag = DESC_TEXT_VIEW_TAG_ID
        
        descTextView.layer.cornerRadius = 5.0
        descTextView.layer.borderWidth = 1.0
        descTextView.layer.borderColor = UIColor.lightGray.cgColor
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdminPromotionsProductCell")
        
        let product = PROMOTION.products[indexPath.row]
        
        if let url = URL(string : serverImageUrl + product.image){
            cell?.imageView?.af_setImage(withURL: url)
        }
        
        cell?.textLabel?.text = product.title
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PROMOTION.products.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField.tag {
        case TITLE_FIELD_TAG_ID:
            self.PROMOTION.title = textField.text! + string
            break
        case PRICE_FIELD_TAG_ID:
            if let price = Float(textField.text! + string){
                self.PROMOTION.price = price
            }
            break
        default:
            break
        }
        
        return true
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let optionsAction = UIContextualAction(style: .normal, title:  "Удалить", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.removeProductFromPromocode(index: indexPath.row)
            success(true)
        })
        optionsAction.backgroundColor = UIColor.red
        
        return UISwipeActionsConfiguration(actions: [optionsAction])
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView.tag == DESC_TEXT_VIEW_TAG_ID {
            self.PROMOTION.desc = textView.text! + text
        }
        
        return true
    }
    
    @objc func changeStatus() {
        let alert = UIAlertController(title : "Выберите вариант", message : nil, preferredStyle : .actionSheet)
        alert.addAction(UIAlertAction(title : "Выключить", style : .default, handler : { (UIAlertAction) -> Void in
            self.PROMOTION.status = "0"
        }))
        alert.addAction(UIAlertAction(title : "Включить", style : .default, handler : { (UIAlertAction) -> Void in
            self.PROMOTION.status = "1"
        }))
        alert.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func productPhotoUpload(_ sender: Any) {
        let alert = UIAlertController(title : "Выберите способ", message : nil, preferredStyle : .actionSheet)
        alert.addAction(UIAlertAction(title : "Снять на камеру", style : .default, handler : { (UIAlertAction) -> Void in self.openCamera()}))
        alert.addAction(UIAlertAction(title : "Выбрать с галереи", style : .default, handler : { (UIAlertAction) -> Void in self.openGallery()}))
        alert.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil ))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openGallery()
    {
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(picker!, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            picker!.allowsEditing = false
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            picker!.cameraCaptureMode = .photo
            self.present(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        uploadImage(image: chosenImage)
        
        dismiss(animated: true, completion: nil)
    }
    
    func uploadImage(image : UIImage){
        SwiftSpinner.show("Загрузка")
        API().uploadImage(image : image , completion: { result in
            SwiftSpinner.hide()
            self.PROMOTION.image = result["file"].stringValue
            
            if (result["success"].boolValue){
                showAlert(result["msg"].stringValue, self)
                DispatchQueue.main.async {
                    self.imageView.image = image
                }
            }else{
                showAlert("Ошибка при загрузке", self)
            }
        })
    }
    
    @IBAction func addProduct(_ sender: Any) {
        API().adminFetchProducts(completion: { products in
            let productsActionSheet = UIAlertController(title : "Выберите продукт", message : nil, preferredStyle : .actionSheet)
            
            for product in products {
                let action = UIAlertAction(title : product.title, style : .default, handler : { (UIAlertAction) -> Void in
                    
                    
                    self.addProductToPromocode(product: product)
                    
                    
                })
                
                productsActionSheet.addAction(action)
            }
            
            productsActionSheet.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil))
            self.present(productsActionSheet, animated: true)
        })
    }
    
    func addProductToPromocode(product : adminFullProduct){
        self.PROMOTION.products.append(product)
        self.tableView.reloadData()
    }
    
    func removeProductFromPromocode(index : Int){
        self.PROMOTION.products.remove(at: index)
        self.tableView.reloadData()
    }
    
    @objc func savePromotion(){
        API().savePromotion(promotion: self.PROMOTION, handle: createPromotion ? .create : .update, completion: { result in
            showAlert(result["msg"].stringValue, self)
        })
    }
}
