//
//  AdminStoreCategoriesVC.swift
//  Eda.Ac
//
//  Created by dingo on 11.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftSpinner


struct AdminCategory {
    var id : String
    var title : String
    var status: String
    var s_id : String
    
    init(){
        id = String()
        title = String()
        status = String()
        s_id = String()
    }
}

class AdminStoreCategoriesVC : UITableViewController {
    
    var Cats = [AdminCategory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getCats()
        
        self.title = "Категории"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Создать", style: .plain, target: self, action: #selector(createCat))

    }
    
    @objc func createCat(){
        let alert = UIAlertController(title: "Создать категорию", message: "Введите новое название", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title : "Создать", style : .default, handler : {(UIAlertAction) -> Void in
            let textField = alert.textFields![0] as UITextField
            
            let new_cat = textField.text!
            
            self.createCatToServer(title: new_cat)
        }))
        alert.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil))
        
        alert.addTextField { (textField) in
            textField.placeholder = "Название"
        }
        
        self.present(alert, animated:true, completion: nil)
    }
    
    func createCatToServer(title : String){
        var jsonCat = JSON()
        
        jsonCat["status"].string = "1"
        jsonCat["title"].string = title
        
        
        //Upload
        let url = URL(string: serverApiURL + "panel/products-categories/insert/?access-token=\(adminAccessToken)")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = try! JSON(jsonCat).rawData()
        
        Alamofire.request(request).responseJSON { response in
            
            response.result.ifSuccess {
                self.getCats()
            }
        }
    }
    
    func getCats(){
        SwiftSpinner.show("Загрузка")
        var url = serverApiURL + "panel/products-categories"
        
        url += "?access-token=\(adminAccessToken)&limit=1000&page=0"
        
        
        Alamofire.request(url).responseJSON { response in
            if let result = response.result.value {
                let json = JSON(result)
                
                self.Cats.removeAll()

                for order in json.arrayValue {
                    let id = order["id"].stringValue
                    let title = order["title"].stringValue
                    let status = order["status"].stringValue
                    let s_id = order["s_id"].stringValue
                    
                    var category = AdminCategory()
                    
                    category.id = id
                    category.title = title
                    category.status = status
                    category.s_id = s_id
                    
                    self.Cats.append(category)
                }
                
                self.tableView.reloadData()
                SwiftSpinner.hide()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdminStoreCategoriesCell")
        
        let cat = self.Cats[indexPath.row]
        
        cell?.textLabel?.text = cat.title
        cell?.detailTextLabel?.text = cat.status == "0" ? "Выключен" : "Включен"
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Cats.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cat = self.Cats[indexPath.row]

        let alert = UIAlertController(title : "Выберите действие", message: nil, preferredStyle : .actionSheet)
        let changeStatusText = cat.status == "0" ? "Включить" : "Выключить"
        alert.addAction(UIAlertAction(title : changeStatusText, style : .default, handler : {(UIAlertAction) -> Void in
            self.Cats[indexPath.row].status = cat.status == "0" ? "1" : "0"
            
            self.UpdateChanges(indexPath.row)
        }))
        alert.addAction(UIAlertAction(title : "Изменить название", style : .default, handler : {(UIAlertAction) -> Void in
            
            self.changeCatName(index: indexPath.row)
            
        }))
        alert.addAction(UIAlertAction(title : "Удалить", style : .destructive, handler : {(UIAlertAction) -> Void in
            self.RemoveCat(indexPath.row)
        }))
        alert.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil))
        self.present(alert, animated: true)
    }
    
    func changeCatName(index : Int){
        let cat = self.Cats[index]

        let change_alert = UIAlertController(title: "Изменить категорию", message: "Введите новое название", preferredStyle: .alert)
        change_alert.addAction(UIAlertAction(title : "Изменить", style : .default, handler : {(UIAlertAction) -> Void in
            let textField = change_alert.textFields![0] as UITextField
            
            self.Cats[index].title = textField.text!
            
            self.UpdateChanges(index)
        }))
        change_alert.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil))

        change_alert.addTextField { (textField) in
            textField.text = cat.title
        }

        self.present(change_alert, animated:true, completion: nil)
    }
    
    
    func RemoveCat(_ index : Int){
        
    }
    
    func UpdateChanges( _ index : Int){
        let cat = self.Cats[index]
        
        var jsonCat = JSON()
            
        jsonCat["id"].string = cat.id
        jsonCat["s_id"].string = cat.s_id
        jsonCat["status"].string = cat.status
        jsonCat["title"].string = cat.title
        
        
        //Upload
        let url = URL(string: serverApiURL + "panel/products-categories/update/?access-token=\(adminAccessToken)&id=\(cat.id)")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = try! JSON(jsonCat).rawData()
        
        Alamofire.request(request).responseJSON { response in
            
            response.result.ifSuccess {
               self.getCats()
            }
        }
    }
}
