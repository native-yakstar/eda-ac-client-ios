//
//  AdminLoginVC.swift
//  Eda.Ac
//
//  Created by dingo on 09.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import OneSignal

class AdminLoginVC : UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //if access token not null present admin panel
        if UserDefaults.standard.string(forKey: "admin-access-token") != nil {
            presentMainAdminPanel()
        }
        
        self.title = "Вход"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "arrow"), style: .plain, target: self, action: #selector(hideVC))
        navigationController?.navigationBar.tintColor = mainColor


        
        loginField.delegate = self
        passwordField.delegate = self
        
    }
    
    @objc func hideVC(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func login(_ sender: Any) {
        auth(login: loginField.text!, password: passwordField.text!)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    func openWithTouchID(){
        
    }
    
    func auth(login : String, password : String){
        let parameters : Parameters = ["username" : login,
                                       "password" : password]
        
        let url = serverApiURL + "panel/auth"
        
        Alamofire.request(url, method : .post, parameters : parameters).responseJSON { response in
            if let result = response.result.value {
                let json = JSON(result)
                
                if json["success"].boolValue {
                    //Save admin-access-token
                    let accessToken = json["token"].stringValue
                    UserDefaults.standard.set(accessToken, forKey: "admin-access-token")
                    UserDefaults.standard.synchronize()
                    
                    OneSignal.sendTag("adminKey", value: accessToken)
                    
                    //set global variable
                    adminAccessToken = accessToken
                    
                    self.presentMainAdminPanel()
                    
                }else{
                    let message = json["message"].stringValue
                    let alert = UIAlertController(title : "Ошибка", message : message, preferredStyle : .alert)
                    alert.addAction(UIAlertAction(title : "Закрыть", style : .default, handler : nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func presentMainAdminPanel(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Admin", bundle: nil)
        let productController = storyBoard.instantiateViewController(withIdentifier: "AdminNavController") as! AdminNavController
        self.present(productController, animated: true, completion: nil)
    }
}
