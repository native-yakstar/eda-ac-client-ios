//
//  AdminChooseProductVC.swift
//  Eda.Ac
//
//  Created by dingo on 11.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSpinner
import SwiftyJSON

struct category {
    var title : String
    var id : String
    
    init() {
        title = String()
        id = String()
    }
}

class AdminChooseProductVC : UIViewController, UITextViewDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UIPopoverControllerDelegate, UINavigationControllerDelegate, SelectCategoryDelegate {
    
    
    var picker : UIImagePickerController? = UIImagePickerController()
    
    var PRODUCT = adminFullProduct()
    
    var createProduct = false
    
    var PRODUCT_TITLE_TAG = 0
    var PRODUCT_DESCR_TAG = 1
    var PRODUCT_COMPOSITION_TAG = 2
    var PRODUCT_WEIGHT_TAG = 3
    var PRODUCT_PRICE_TAG = 4
    
    @IBOutlet weak var productTitleField: UITextField!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productDescTextView: UITextView!
    @IBOutlet weak var productCompositionTextView: UITextView!
    @IBOutlet weak var productWeightField: UITextField!
    @IBOutlet weak var productPriceField: UITextField!
    @IBOutlet weak var productCategoryBtn: UIButton!
    @IBOutlet weak var removeImageBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        productTitleField.delegate = self
        productTitleField.tag = self.PRODUCT_TITLE_TAG
        
        productDescTextView.delegate = self
        productDescTextView.tag = self.PRODUCT_TITLE_TAG
        productDescTextView.layer.cornerRadius = 5.0
        productDescTextView.layer.borderWidth = 1.0
        productDescTextView.layer.borderColor = UIColor.darkGray.cgColor
        
        productCompositionTextView.delegate = self
        productCompositionTextView.tag = self.PRODUCT_COMPOSITION_TAG
        productCompositionTextView.layer.cornerRadius = 5.0
        productCompositionTextView.layer.borderWidth = 1.0
        productCompositionTextView.layer.borderColor = UIColor.darkGray.cgColor
        
        productWeightField.delegate = self
        productWeightField.tag = self.PRODUCT_WEIGHT_TAG
        
        productPriceField.delegate = self
        productPriceField.tag = self.PRODUCT_PRICE_TAG
        
        
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: createProduct ? "Опубликовать" : "Сохр.", style: .plain, target: self, action: #selector(saveProduct))

        picker?.delegate = self
        
        //Set UI values
        self.removeImageBtn.setTitle(createProduct ? "Добавить" : "Изменить", for: .normal)
        self.productTitleField.text = self.PRODUCT.title
        self.productDescTextView.text = self.PRODUCT.desc
        self.productCompositionTextView.text = self.PRODUCT.composition
        self.productWeightField.text = self.PRODUCT.weight
        self.productPriceField.text = String(self.PRODUCT.price)
        
        if let url = URL(string: serverImageUrl + self.PRODUCT.image) {
            self.productImage.af_setImage(withURL: url)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //Обновление в PRODUCT
    
    //для text-view
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let text = textView.text! + text
        
        switch textView.tag {
        case PRODUCT_DESCR_TAG:
            
            self.PRODUCT.desc = textView.text! + text
            
            break
        case PRODUCT_COMPOSITION_TAG:
            
            self.PRODUCT.composition = textView.text! + text
            
            break
        default:
            break
        }
        
        return true
    }
    
    //для text-field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField.tag {
        case PRODUCT_TITLE_TAG:
            self.PRODUCT.title = textField.text! + string
            break
        case PRODUCT_WEIGHT_TAG:
            self.PRODUCT.weight = textField.text! + string
            break
        case PRODUCT_PRICE_TAG:
            self.PRODUCT.price = Float(textField.text! + string)!
            break
        default:
            break
        }
        return true
    }
    
    @IBAction func productCategoryChange(_ sender: Any) {
        //AdminChooseCategories
        let storyBoard: UIStoryboard = UIStoryboard(name: "Admin", bundle: nil)
        
        let chooseCatController = storyBoard.instantiateViewController(withIdentifier: "AdminChooseCategories") as! AdminChooseCategories
        
        chooseCatController.delegate = self
        
        self.navigationController?.pushViewController(chooseCatController, animated: true)
    }
    
    func selectedCat(cat: category) {
        self.PRODUCT.category_id = cat.id
        self.PRODUCT.category_title = cat.title
        
        self.productCategoryBtn.setTitle(cat.title, for: .normal)
    }
    
    // IMAGE HANDLER
    
    @IBAction func productPhotoImageRemove(_ sender: Any) {
        self.PRODUCT.image = String()
        self.productImage.image = UIImage()
    }
    
    @IBAction func productPhotoUpload(_ sender: Any) {
        let alert = UIAlertController(title : "Выберите способ", message : nil, preferredStyle : .actionSheet)
        alert.addAction(UIAlertAction(title : "Снять на камеру", style : .default, handler : { (UIAlertAction) -> Void in self.openCamera()}))
        alert.addAction(UIAlertAction(title : "Выбрать с галереи", style : .default, handler : { (UIAlertAction) -> Void in self.openGallery()}))
        alert.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil ))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openGallery()
    {
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(picker!, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            picker!.allowsEditing = false
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            picker!.cameraCaptureMode = .photo
            self.present(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
    }

    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        uploadToServer(image: chosenImage)

        dismiss(animated: true, completion: nil)
    }
    
    func uploadToServer(image : UIImage){
        
        API().uploadImage(image : image , completion: { result in
            self.PRODUCT.image = result["file"].stringValue
            
            if (result["success"].boolValue){
                showAlert(result["msg"].stringValue, self)
                DispatchQueue.main.async {
                    self.productImage.image = image
                }
            }else{
                showAlert("Ошибка при загрузке", self)
            }
        })

    }
    
    @objc func saveProduct(){
        if createProduct {
//            API().adminInsertNewProduct(product: PRODUCT, completion : { result in
//                showAlert(result["msg"].stringValue, self)
//            })
        }else{
            API().adminUpdateProductToServer(product: PRODUCT, completion : { result in
                showAlert(result["msg"].stringValue, self)
            })
        }
    }
    
}
