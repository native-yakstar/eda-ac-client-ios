//
//  AdminStoreConfigVC.swift
//  Eda.Ac
//
//  Created by dingo on 14.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftyJSON

struct AdminStoreInfo {
    
    var id : String
    var title : String
    var desc : String
    var delivery : String
    var image : String
    var hours_from : String
    var hours_to : String
    var address : String
    var cashless_payment : String
    var lat : String
    var lon : String
    var min_price_order : String
    var delivery_time : String
    var background : String
    var phone : String
    
    init() {
        id = String()
        title = String()
        desc = String()
        delivery = String()
        image = String()
        hours_from = String()
        hours_to = String()
        address = String()
        cashless_payment = String()
        lat = String()
        lon = String()
        min_price_order = String()
        delivery_time = String()
        background = String()
        phone = String()
    }
}

class AdminStoreConfigVC : UITableViewController, UIImagePickerControllerDelegate, UIPopoverControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var Store = AdminStoreInfo()
    
    @IBOutlet weak var storeField: UITextField!
    @IBOutlet weak var storeDesc: UITextField!
    @IBOutlet weak var storeDelieve: UITextField!
    @IBOutlet weak var storeHoursFrom: UITextField!
    @IBOutlet weak var storeHoursTo: UITextField!
    @IBOutlet weak var storeAddress: UITextField!
    @IBOutlet weak var storepayType: UITextField!
    @IBOutlet weak var storeLat: UITextField!
    @IBOutlet weak var storeLon: UITextField!
    @IBOutlet weak var storeMinAmount: UITextField!
    @IBOutlet weak var storeDelieveTime: UITextField!
    @IBOutlet weak var storePhoneNumber: UITextField!
    @IBOutlet weak var storeLogo: UIImageView!
    @IBOutlet weak var storeBg: UIImageView!
    
    let STORE_TITLE_TAG_ID = 0
    let STORE_DESC_TAG_ID = 1
    let STORE_DELIEVE_TAG_ID = 2
    let STORE_HOURS_FROM_TAG_ID = 3
    let STORE_HOURS_TO_TAG_ID = 4
    let STORE_ADDRESS_TAG_ID = 5
    let STORE_PAY_TYPE_TAG_ID = 6
    let STORE_LAT_TAG_ID = 7
    let STORE_LON_FIELD_TAG_ID = 8
    let STORE_MIN_AMOUNT_TAG_ID = 9
    let STORE_DELIEVE_TIME_TAG_ID = 10
    let STORE_PHONE_NUMBER_TAG_ID = 11
    
    let payTypes = ["Наличная оплата", "Безналичная оплата"]
    
    var logoImageUploadingFlag = false
    var backgroundImageUploadingFlag = false
    
    var picker : UIImagePickerController? = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker?.delegate = self
        
        storeField.delegate = self
        storeField.tag = STORE_TITLE_TAG_ID
        
        storeDesc.delegate = self
        storeDesc.tag = STORE_DESC_TAG_ID
        
        storeDelieve.delegate = self
        storeDelieve.tag = STORE_DELIEVE_TAG_ID
        
        storeHoursFrom.delegate = self
        storeHoursFrom.tag = STORE_HOURS_FROM_TAG_ID
        
        storeHoursTo.delegate = self
        storeHoursTo.tag = STORE_HOURS_TO_TAG_ID
        
        storeAddress.delegate = self
        storeAddress.tag = STORE_ADDRESS_TAG_ID
        
        storepayType.delegate = self
        storepayType.tag = STORE_PAY_TYPE_TAG_ID
        
        storeLat.delegate = self
        storeLat.tag = STORE_LAT_TAG_ID
        
        storeLon.delegate = self
        storeLon.tag = STORE_LON_FIELD_TAG_ID
        
        storeMinAmount.delegate = self
        storeMinAmount.tag = STORE_MIN_AMOUNT_TAG_ID
        
        storeDelieveTime.delegate = self
        storeDelieveTime.tag = STORE_DELIEVE_TIME_TAG_ID
        
        
        storePhoneNumber.delegate = self
        storePhoneNumber.tag = STORE_PHONE_NUMBER_TAG_ID
        
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Сохр.", style: .plain, target: self, action: #selector(setUpdates))
        
        // Set Fields Input View
        let storePayPicker = UIPickerView()
        storePayPicker.delegate = self
        storePayPicker.tag = STORE_PAY_TYPE_TAG_ID
        storepayType.inputView = storePayPicker

        
        let hoursToPicker :UIDatePicker = UIDatePicker()
        hoursToPicker.datePickerMode = UIDatePickerMode.time
        storeHoursTo.inputView = hoursToPicker
        hoursToPicker.tag = STORE_HOURS_TO_TAG_ID
        
        
        hoursToPicker.addTarget(self, action: #selector(self.datePickerFromValueChanged(sender:)), for: UIControlEvents.valueChanged)
        
        let hoursFromPicker :UIDatePicker = UIDatePicker()
        hoursFromPicker.datePickerMode = UIDatePickerMode.time
        storeHoursFrom.inputView = hoursFromPicker
        hoursFromPicker.tag = STORE_HOURS_FROM_TAG_ID
        hoursFromPicker.addTarget(self, action: #selector(self.datePickerFromValueChanged(sender:)), for: UIControlEvents.valueChanged)
        
        
        
        API().getStore(completion: { store in
            self.Store = store
            self.initView()
        })
    }
    
    func initView(){
        storeField.text = Store.title
        
        storeDesc.text = Store.desc
        
        storeDelieve.text = Store.delivery
        
        //*** Specific input view
        storeHoursFrom.text = Store.hours_from
        
        //*** Specific input view
        storeHoursTo.text = Store.hours_to
        
        storeAddress.text = Store.address
        
        //*** Specific input view
        storepayType.text = Store.cashless_payment == "0" ? "Наличная оплата" : "Безналичная оплата"
        
        
        storeLat.text = Store.lat
        
        storeLon.text = Store.lon
        
        storeMinAmount.text = Store.min_price_order
        
        //*** Specific input view
        storeDelieveTime.text = Store.delivery_time
        
        storePhoneNumber.text = Store.phone
        
        if let url = URL(string : serverImageUrl + Store.image){
            self.storeLogo.af_setImage(withURL: url)
        }
        if let url = URL(string : serverImageUrl + Store.background){
            self.storeBg.af_setImage(withURL: url)
        }
        
    }
    
    @IBAction func storeLogoUpload(_ sender: Any) {
        logoImageUploadingFlag = true
        backgroundImageUploadingFlag = false
        photoUpload()
    }
    
    @IBAction func storeLogoRemove(_ sender: Any) {
        self.Store.image = String()
        self.storeLogo.image = UIImage()
    }
    
    @IBAction func storeBackgroungUpload(_ sender: Any) {
        logoImageUploadingFlag = false
        backgroundImageUploadingFlag = true
        photoUpload()
    }
    
    @IBAction func storeBackgroundRemove(_ sender: Any) {
        self.Store.background = String()
        self.storeBg.image = UIImage()
    }
    
    func photoUpload() {
        let alert = UIAlertController(title : "Выберите способ", message : nil, preferredStyle : .actionSheet)
        alert.addAction(UIAlertAction(title : "Снять на камеру", style : .default, handler : { (UIAlertAction) -> Void in self.openCamera()}))
        alert.addAction(UIAlertAction(title : "Выбрать с галереи", style : .default, handler : { (UIAlertAction) -> Void in self.openGallery()}))
        alert.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil ))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openGallery()
    {
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(picker!, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            picker!.allowsEditing = false
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            picker!.cameraCaptureMode = .photo
            self.present(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        uploadImage(image: chosenImage)
        
        dismiss(animated: true, completion: nil)
    }
    
    func uploadImage(image : UIImage){
        SwiftSpinner.show("Загрузка")
        API().uploadImage(image : image , completion: { result in
            SwiftSpinner.hide()
            
            
            if (result["success"].boolValue){
                showAlert(result["msg"].stringValue, self)
                
                if self.logoImageUploadingFlag && !self.backgroundImageUploadingFlag {
                    //Save logo
                    self.Store.image = result["file"].stringValue
                    
                    DispatchQueue.main.async {
                        self.storeLogo.image = image
                    }
                    
                } else {
                    //Save background
                    self.Store.background = result["file"].stringValue

                    DispatchQueue.main.async {
                        self.storeBg.image = image
                    }
                    
                }
                
                
            }else{
                showAlert("Ошибка при загрузке", self)
            }
        })
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField.tag {
        case STORE_TITLE_TAG_ID:
            self.Store.title = textField.text! + string
            break
        case STORE_DESC_TAG_ID:
            self.Store.desc = textField.text! + string
            break
        case STORE_DELIEVE_TAG_ID:
            self.Store.delivery = textField.text! + string
            break
        case STORE_HOURS_TO_TAG_ID:
            self.Store.hours_to = textField.text! + string
            break
        case STORE_ADDRESS_TAG_ID:
            self.Store.address = textField.text! + string
            break
        case STORE_LAT_TAG_ID:
            self.Store.lat = textField.text! + string
            break
        case STORE_LON_FIELD_TAG_ID:
            self.Store.lon = textField.text! + string
            break
        case STORE_MIN_AMOUNT_TAG_ID:
            self.Store.min_price_order = textField.text! + string
            break
        case STORE_DELIEVE_TIME_TAG_ID:
            self.Store.delivery_time = textField.text! + string
            break
        case STORE_PHONE_NUMBER_TAG_ID:
            self.Store.phone = textField.text! + string
            break
        default:
            break
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
    }
    
    // PICKER VIEW
    

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == self.STORE_PAY_TYPE_TAG_ID {
            return self.payTypes.count
        } else {
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == self.STORE_PAY_TYPE_TAG_ID {
            return self.payTypes[row]
        } else {
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == self.STORE_PAY_TYPE_TAG_ID {
            self.Store.cashless_payment = row == 0 ? "0" : "1"

            self.storepayType.text = self.payTypes[row]
        }
    }
    
    @objc func datePickerFromValueChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "RU_ru")
        dateFormatter.dateFormat = "HH:mm:ss"
        
        let gregorian = Calendar(identifier: .gregorian)
        var components = gregorian.dateComponents([.hour, .minute, .second], from: sender.date)
        
        components.second = 0

        let date = gregorian.date(from: components)!

        if sender.tag == STORE_HOURS_TO_TAG_ID {
            self.storeHoursTo.text = dateFormatter.string(from: date)
            self.Store.hours_to = dateFormatter.string(from: date)

        } else if sender.tag == STORE_HOURS_FROM_TAG_ID {
            self.storeHoursFrom.text = dateFormatter.string(from: date)
            self.Store.hours_from = dateFormatter.string(from: date)
        }
    }
    
    
    
    // TABLE VIEW
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @objc func setUpdates(){
        SwiftSpinner.show("Обновление")
        API().updateStore(store: self.Store, completion : { result in
            showAlert(result["msg"].stringValue, self)
            SwiftSpinner.hide()
        })
    }
    
}
