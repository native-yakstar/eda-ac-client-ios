//
//  AdminStoreGalleryCell.swift
//  Eda.Ac
//
//  Created by dingo on 14.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit

class AdminStoreGalleryCell: UITableViewCell {
    
    @IBOutlet weak var storeImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
