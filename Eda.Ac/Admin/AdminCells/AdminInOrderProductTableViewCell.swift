//
//  AdminInOrderProductTableViewCell.swift
//  Eda.Ac
//
//  Created by dingo on 11.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit

class AdminInOrderProductTableViewCell: UITableViewCell {

    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productCount: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productTotalCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
