//
//  AdminOrdersTableViewCell.swift
//  Eda.Ac
//
//  Created by dingo on 10.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit

class AdminOrdersTableViewCell: UITableViewCell {

    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var customerAddress: UILabel!
    @IBOutlet weak var orderCreatedDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
