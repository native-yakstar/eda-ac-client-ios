//
//  AdminPromotionCell.swift
//  Eda.Ac
//
//  Created by dingo on 13.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit

class AdminPromotionCell : UITableViewCell {
    
    @IBOutlet weak var promotionImage: UIImageView!
    @IBOutlet weak var promotionTitle: UILabel!
    @IBOutlet weak var promotionPrice: UILabel!
    @IBOutlet weak var promotionStatus: UILabel!
    @IBOutlet weak var promotionProductCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

