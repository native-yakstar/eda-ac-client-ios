//
//  AdminMainSelectVC.swift
//  Eda.Ac
//
//  Created by dingo on 10.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import OneSignal

enum AdminPages {
    case orders
    case productsCategories
    case products
    case promocodes
    case promotions
    case settings
    case storesGallery
}

class AdminMainSelectVC : UITableViewController {
    
    var pages = [String]()
    
    override func viewDidLoad() {
        print(adminAccessToken)
        self.pages = ["Заказы", "Категории товаров", "Товары", "Промокоды", "Акции", "Настройки", "Галерея"]
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "adminMainSelectCell")
        
        cell?.textLabel?.text = self.pages[indexPath.row]
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pages.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 0:
            presentVC(page: .orders)
            break
        case 1:
            presentVC(page: .productsCategories)
            break
        case 2:
            presentVC(page: .products)
            break
        case 3:
            presentVC(page: .promocodes)
            break
        case 4:
            presentVC(page: .promotions)
            break
        case 5:
            presentVC(page: .settings)
            break
        case 6:
            presentVC(page: .storesGallery)
            break
        default:
            
            break
        }
    }
    
    func presentVC(page : AdminPages) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Admin", bundle: nil)
        
        switch page {
            case .orders:
                let pageController = storyBoard.instantiateViewController(withIdentifier: "AdminOrdersVC") as! AdminOrdersVC
                
                self.navigationController?.pushViewController(pageController, animated: true)
                
                break
            case .productsCategories:
                let pageController = storyBoard.instantiateViewController(withIdentifier: "AdminStoreCategoriesVC") as! AdminStoreCategoriesVC
                self.navigationController?.pushViewController(pageController, animated: true)

                break
            case .products:
                let pageController = storyBoard.instantiateViewController(withIdentifier: "AdminProductsVC") as! AdminProductsVC
                self.navigationController?.pushViewController(pageController, animated: true)

                break
            case .promocodes:
                let pageController = storyBoard.instantiateViewController(withIdentifier: "AdminChoosePromocodesVC") as! AdminChoosePromocodesVC
                self.navigationController?.pushViewController(pageController, animated: true)

                break
            case .promotions:
                let pageController = storyBoard.instantiateViewController(withIdentifier: "AdminPromotionsVC") as! AdminPromotionsVC
                self.navigationController?.pushViewController(pageController, animated: true)

                break
            case .settings:
                let pageController = storyBoard.instantiateViewController(withIdentifier: "AdminStoreConfigVC") as! AdminStoreConfigVC
                self.navigationController?.pushViewController(pageController, animated: true)

                break
            case .storesGallery:
                let pageController = storyBoard.instantiateViewController(withIdentifier: "AdminStoreGalleryVC") as! AdminStoreGalleryVC
                self.navigationController?.pushViewController(pageController, animated: true)

                break
        }
    }
    
    @IBAction func optionsButtons(_ sender: Any) {
        let alert = UIAlertController(title : "Выберите вариант", message : nil, preferredStyle : .actionSheet)
        alert.addAction(UIAlertAction(title : "Выйти с панели", style : .destructive, handler : { (UIAlertAction) ->  Void in
            UserDefaults.standard.removeObject(forKey: "admin-access-token")
            UserDefaults.standard.synchronize()
            
            OneSignal.sendTag("adminKey", value: "")
            
            self.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title : "Отмена", style : .cancel , handler : nil))
        self.present(alert, animated : true)
    }
}
