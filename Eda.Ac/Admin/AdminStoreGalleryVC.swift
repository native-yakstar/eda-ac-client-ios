//
//  AdminStoreGalleryVC.swift
//  Eda.Ac
//
//  Created by dingo on 14.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import SwiftyJSON
import SwiftSpinner

struct AdminStoreImage {
    var id : String
    var title : String
    var image : String
    
    init() {
        id = String()
        title = String()
        image = String()
    }
}

class AdminStoreGalleryVC : UITableViewController, UIImagePickerControllerDelegate, UIPopoverControllerDelegate, UINavigationControllerDelegate {
    
    var images = [AdminStoreImage]()
    var picker : UIImagePickerController? = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        picker?.delegate = self

        getImages()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Добавить", style: .plain, target: self, action: #selector(photoUpload))
    }
    
    func getImages(){
        SwiftSpinner.show("Загрузка")
        API().getStoreImages(completion: { images in
            SwiftSpinner.hide()
            self.images = images
            self.tableView.reloadData()
        })
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdminStoreGalleryCell") as! AdminStoreGalleryCell
        
        cell.storeImage.af_setImage(withURL: URL(string: serverImageUrl + self.images[indexPath.row].image)!)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return images.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        let alert = UIAlertController(title : "Выберите вариант", message : nil, preferredStyle : .actionSheet)
        alert.addAction(UIAlertAction(title : "Удалить", style : .destructive, handler : { (UIAlertAction) -> Void in
            SwiftSpinner.show("Удаление")
            API().removeStoreImages(image: self.images[indexPath.row], completion: { result in
                SwiftSpinner.hide()
                showAlert(result["msg"].stringValue, self)
                
                self.getImages()
            })
        }))
        alert.addAction(UIAlertAction(title : "Закрыть", style : .cancel, handler : nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func photoUpload() {
        let alert = UIAlertController(title : "Выберите способ", message : nil, preferredStyle : .actionSheet)
        alert.addAction(UIAlertAction(title : "Снять на камеру", style : .default, handler : { (UIAlertAction) -> Void in self.openCamera()}))
        alert.addAction(UIAlertAction(title : "Выбрать с галереи", style : .default, handler : { (UIAlertAction) -> Void in self.openGallery()}))
        alert.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil ))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openGallery()
    {
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(picker!, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            picker!.allowsEditing = false
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            picker!.cameraCaptureMode = .photo
            self.present(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        uploadImage(image: chosenImage)
        
        dismiss(animated: true, completion: nil)
    }
    
    func uploadImage(image : UIImage){
        SwiftSpinner.show("Загрузка")
        API().uploadImage(image : image , completion: { result in
            SwiftSpinner.hide()
            self.getImages()

            if (result["success"].boolValue){
                
                var storeImage = AdminStoreImage()
                storeImage.image = result["file"].stringValue
                
                API().insertStoreImages(image: storeImage, completion: { result in
                    showAlert(result["msg"].stringValue, self)
                    
                    self.getImages()
                })
                
            }else{
                showAlert("Ошибка при загрузке", self)
            }
        })
    }
}
