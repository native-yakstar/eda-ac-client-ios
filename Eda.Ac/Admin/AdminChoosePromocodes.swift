//
//  AdminChoosePromocodes.swift
//  Eda.Ac
//
//  Created by dingo on 12.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import SwiftSpinner

class AdminChoosePromocodesVC : UITableViewController {
    
    var promocodes = [PROMOCODE]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getPromocodes()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Добавить промокод", style: .plain, target: self, action: #selector(createPromocode))
    }
    
    
    func getPromocodes(){
        SwiftSpinner.show("Загрузка")
        API().getPromocodes(completion : { promocodes in
            self.promocodes = promocodes
            self.tableView.reloadData()
            SwiftSpinner.hide()
        })
    }
    
    @objc func createPromocode(){
        let alert = UIAlertController(title : "Выберите тип продукта", message : nil, preferredStyle : .actionSheet)
        alert.addAction(UIAlertAction(title : "Товар в подарок", style : .default, handler : { (UIAlertAction) -> Void in
            self.productInGift()
        }))
        alert.addAction(UIAlertAction(title : "Скидка на итоговую цену", style : .default, handler : { (UIAlertAction) -> Void in
            self.DiscountForTotalCount()
        }))
        alert.addAction(UIAlertAction(title : "Скидка на определенный товар", style : .default, handler : { (UIAlertAction) -> Void in
            self.DiscountForSpecificProduct()
        }))
        alert.addAction(UIAlertAction(title : "Закрыть", style : .cancel, handler : nil))
        
        
        
        self.present(alert, animated: true)
    }
    
    func productInGift(){
        //Получение списка товаров
        API().adminFetchProducts(completion: { products in
            
            let productsActionSheet = UIAlertController(title : "Выберите продукт", message : nil, preferredStyle : .actionSheet)
            
            for product in products {
                let action = UIAlertAction(title : product.title, style : .default, handler : { (UIAlertAction) -> Void in
                    
                    var promocode = PROMOCODE(promocodeType.productInGift)
                    promocode.p_id = product.id
                    promocode.typeString = "1"
                    self.setCodeForPromocode(promocode, true, completion : { Void in })
                    
                })
                
                productsActionSheet.addAction(action)
            }
            
            productsActionSheet.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil))
            self.present(productsActionSheet, animated: true)
        })
        
    }
    
    func DiscountForSpecificProduct(){
        //
        
        API().adminFetchProducts(completion: { products in
            
            let productsActionSheet = UIAlertController(title : "Выберите продукт", message : nil, preferredStyle : .actionSheet)
            
            for product in products {
                let action = UIAlertAction(title : product.title, style : .default, handler : { (UIAlertAction) -> Void in
                    
                    var promocode = PROMOCODE(promocodeType.DiscountForSpecificProduct)
                    promocode.p_id = product.id
                    promocode.typeString = "3"
                    
                    self.selectPromocodeType(_promocode: promocode)
                    
                })
                
                productsActionSheet.addAction(action)
            }
            
            productsActionSheet.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil))
            self.present(productsActionSheet, animated: true)
        })
        
    }
    
    
    func DiscountForTotalCount(){
        var promocode = PROMOCODE(promocodeType.DiscountForTotalCount)
        promocode.statusActive = true
        promocode.typeString = "2"
        
        selectPromocodeType(_promocode: promocode)
    }
    
    func selectPromocodeType(_promocode : PROMOCODE){
        var promocode = _promocode
        
        let selectAlert = UIAlertController(title : "Выберите тип скидки", message : nil, preferredStyle : .actionSheet)
        selectAlert.addAction(UIAlertAction(title : "Процент", style : .default, handler : { (UIAlertAction) -> Void in
            promocode.percent_promocode = true
            self.setValueForPromocode(promocode)
            
        }))
        
        selectAlert.addAction(UIAlertAction(title : "Сумма", style : .default, handler : { (UIAlertAction) -> Void in
            
            promocode.percent_promocode = false
            self.setValueForPromocode(promocode)
            
        }))
        
        selectAlert.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil))
        
        self.present(selectAlert, animated: true)
    }
    
    func setValueForPromocode(_ promocode : PROMOCODE){
        var _promocode = promocode
        let alert = UIAlertController(title: "Введите значение", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title : "Готово", style : .default, handler : {(UIAlertAction) -> Void in
            let textField = alert.textFields![0] as UITextField
            
            if _promocode.percent_promocode {
                _promocode.percent = Int(textField.text!)!
            } else {
                _promocode.discount = Int(textField.text!)!
            }
            
            self.setCodeForPromocode(_promocode, true, completion : { m_promocode in } )
            
            
        }))
        alert.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil))
        
        alert.addTextField { (textField) in
            textField.placeholder = "Процент или сумма"
            textField.keyboardType = .decimalPad
        }
        
        self.present(alert, animated:true, completion: nil)
    }
    
    func setCodeForPromocode(_ promocode : PROMOCODE, _ publish : Bool, completion: @escaping (_ m_promocode : PROMOCODE) -> Void) {
        var _promocode = promocode
        let alert = UIAlertController(title: "Введите промокод", message: "Введите код промокода", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title : "Готово", style : .default, handler : {(UIAlertAction) -> Void in
            let textField = alert.textFields![0] as UITextField
            
            _promocode.code = textField.text!
            
            if publish {
                API().handlePromocode(promocode: _promocode, handle : .create, completion: { result in
                    showAlert(result["msg"].stringValue, self)
                    self.getPromocodes()
                })
            } else {
                completion(_promocode)
            }
        }))
        alert.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil))
        
        alert.addTextField { (textField) in
            textField.placeholder = "КОД"
        }
        
        self.present(alert, animated:true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdminChoosePromocodesCell")
        
        let promocode = promocodes[indexPath.row]
        
        cell?.textLabel?.text = promocode.code
        
        var productTypeText = String()
        
        switch promocode.type {
        case .DiscountForSpecificProduct :
            productTypeText = "Скидка на определенный продукт"
            break
        case .DiscountForTotalCount :
            productTypeText = "Скидка на итоговую цену"
            break
        case .productInGift :
            productTypeText = "Продукт в подарок"
            break
        case .Unknown :
            productTypeText = "Ошибка при определении скидки"
            break
        }
        
        cell?.detailTextLabel?.text = productTypeText
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return promocodes.count
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        self.showPromocodeHandleButton(index: indexPath.row)
    }
    
    func showPromocodeHandleButton(index : Int){
        let promocode = self.promocodes[index]
        let alert = UIAlertController(title : promocode.code, message : nil, preferredStyle : .actionSheet)
        alert.addAction(UIAlertAction(title : "Информация", style : .default, handler : { (UIAlertAction) -> Void in self.showPromocodeInfo(index)}))
        
        alert.addAction(UIAlertAction(title : "Изменить код", style : .default, handler : { (UIAlertAction) -> Void in
            self.setCodeForPromocode(self.promocodes[index], false, completion : { m_promocode in
                API().handlePromocode(promocode: m_promocode, handle: .change, completion: { result in
                    showAlert(result["msg"].stringValue, self)
                    self.getPromocodes()

                })
            })
        }))
        alert.addAction(UIAlertAction(title : self.promocodes[index].statusActive ? "Выключить" : "Включить", style : .default, handler : { (UIAlertAction) -> Void in
            var _promocode = self.promocodes[index]
            
            _promocode.statusActive = !_promocode.statusActive
            
            API().handlePromocode(promocode: _promocode, handle: .change, completion: { result in
                showAlert(result["msg"].stringValue, self)
                self.getPromocodes()
            })
            
        }))
        
        alert.addAction(UIAlertAction(title : "Удалить", style : .destructive, handler : { (UIAlertAction) -> Void in self.removePromocode(index)}))
        
        alert.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showPromocodeInfo(_ index : Int){
        let promocode = self.promocodes[index]
        
        var productTypeText = String()
        
        switch promocode.type {
        case .DiscountForSpecificProduct :
            productTypeText = "Скидка на определенный продукт"
            break
        case .DiscountForTotalCount :
            productTypeText = "Скидка на итоговую цену"
            break
        case .productInGift :
            productTypeText = "Продукт в подарок"
            break
        case .Unknown :
            productTypeText = "Ошибка при определении скидки"
            break
        }
        
        let alert = UIAlertController(title : promocode.code, message : "Номер: \(promocode.id)\n Код: \(promocode.code)\n Тип промокода: \(productTypeText)\n Процент: \(promocode.percent)\n Сумма: \(promocode.discount)\n Товар: \(promocode.product_title)\n Статус: \(promocode.statusActive ? "Включен" : "Выключен")" , preferredStyle : .alert)
        alert.addAction(UIAlertAction(title : "Закрыть", style : .cancel, handler : nil))
        self.present(alert, animated: true)
        
    }
    
    func removePromocode(_ index : Int){
        let alert = UIAlertController(title : "Удалить промокод?", message : nil, preferredStyle : .alert)
        alert.addAction(UIAlertAction(title : "Удалить", style : .destructive, handler : { (UIAlertAction) -> Void in
            API().handlePromocode(promocode: self.promocodes[index], handle: .remove, completion: { result in
                showAlert(result["msg"].stringValue, self)
                self.getPromocodes()

            })
        }))
        alert.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil))
        self.present(alert, animated: true, completion: nil)
    }
}
