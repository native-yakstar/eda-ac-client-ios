//
//  AdminOrdersVC.swift
//  Eda.Ac
//
//  Created by dingo on 10.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftSpinner

struct OrdersMin {
    var id : String
    var customer_name : String
    var customer_address : String
    var order_create_date : String
    var order_status : OrderStatus
    var customer_phone : String
}

enum OrderStatus {
    case inActive
    case active
    case completed
    case cancelled
}



class AdminOrdersVC : UITableViewController {
    
    var Orders = [OrdersMin]()
    
    var PAGINATION_LIMIT = 1000
    var FETCHING_DATA = false
    
    var OrdersSTATUS = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Статус", style: .plain, target: self, action: #selector(changeStatus))
        
        refreshControl = UIRefreshControl()
        refreshControl?.attributedTitle = NSAttributedString(string: "Потяните для обновления")
        refreshControl?.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControlEvents.valueChanged)
        
        self.title = ADMIN_ORDER_STATUSES[OrdersSTATUS]

        
        getOrders(completion: { (orders) -> Void in
            self.Orders = orders
            self.tableView.reloadData()
        })
    }
    
    @objc func refresh(sender:AnyObject) {
        getOrders(completion: { (orders) -> Void in
            self.Orders = orders
            self.tableView.reloadData()
        })
    }
    
    @objc func changeStatus(){
        let alert = UIAlertController(title : "Выбрать статус", message : nil, preferredStyle : .actionSheet)
        for index in 0 ..< ADMIN_ORDER_STATUSES.count {
            let orderStatus = ADMIN_ORDER_STATUSES[index]
            alert.addAction(UIAlertAction(title : orderStatus, style : .default, handler : {(UIAlertAction) -> Void in
                
                self.OrdersSTATUS = index
                self.title = ADMIN_ORDER_STATUSES[self.OrdersSTATUS]

                self.getOrders(completion: { (orders) -> Void in
                    self.Orders = orders
                    self.tableView.reloadData()
                })
                
            }))
        }
        alert.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func getOrders(completion: @escaping (_ orders : [OrdersMin]) -> Void) {
        SwiftSpinner.show("Загрузка")
        //Заблокировать VIEW
        if !FETCHING_DATA {
            self.FETCHING_DATA = true
            
            var url = serverApiURL + "panel/orders"
            

            url += "?access-token=\(adminAccessToken)&page=0&limit=\(self.PAGINATION_LIMIT)&status=\(self.OrdersSTATUS)"
            
            print("Загрузка \(url)")
            var orders = [OrdersMin]()

            Alamofire.request(url).responseJSON { response in
                if let result = response.result.value {
                    let json = JSON(result)
                    
                    
                    print(json.arrayValue.count)
                    if json.arrayValue.count == 0 {
                    } else {
                        for order in json.arrayValue {
                            let id = order["id"].stringValue
                            let customer_name = order["customer_name"].stringValue
                            let customer_address = order["customer_address"].stringValue
                            let order_create_date = order["datetime"].stringValue
                            let customer_phone = order["customer_phone"].stringValue
                            var orderStatus = OrderStatus.inActive
                            
                            switch order["status"].stringValue {
                                case "0":
                                    orderStatus = .inActive
                                    break
                                case "1":
                                    orderStatus = .active
                                    break
                                case "2":
                                    orderStatus = .completed
                                    break
                                case "3":
                                    orderStatus = .cancelled
                                    break
                                default:
                                    orderStatus = .inActive
                                    break
                            }
                            
                            let _order = OrdersMin.init(id: id, customer_name: customer_name, customer_address: customer_address, order_create_date: order_create_date, order_status: orderStatus, customer_phone : customer_phone)
                         
                            orders.append(_order)
                        }
                        
                    }

                    self.FETCHING_DATA = false
                }
                
                completion(orders)
                self.refreshControl?.endRefreshing()
                SwiftSpinner.hide()
            }
        }
    }
    

    
    
//    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//        //Если это первый запуск
//        if self.Orders.count != 0 {
//            //Если все заказы не покрывают экран то не загружать
//            let table_view_height = self.tableView.frame.height
//            let cells_height = 100 * CGFloat(self.Orders.count)
//
//            let screen_download_allow = cells_height > table_view_height
//
//
//            if (screen_download_allow){
//                if indexPath.row == self.Orders.count - 1 {
//                    //Если это последняя ячейка
//                    getOrders()
//                    print("Загрузить")
//
//                }
//            }
//
//        }
//    }
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdminOrdersTableViewCell") as! AdminOrdersTableViewCell
        
        
        let order = self.Orders[indexPath.row]
        
        cell.orderIdLabel.text = "Номер заказа: \(order.id)"
        cell.customerNameLabel.text = "Имя: \(order.customer_name)"
        cell.customerAddress.text = "Адрес: \(order.customer_address)"
        
        //refactoring date
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "RU_ru")

        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        var order_created_date = Date()
        
        if let date = dateFormatter.date(from: order.order_create_date) {
            dateFormatter.dateFormat = "HH:mm,  dd MMM"
            order_created_date = date
            cell.orderCreatedDate.text = "Создан: \(dateFormatter.string(from: date))"

        }

        
        
        let currentDate = Date()
        //Если заказ создан 2 часа назад и активен то надо его сделать красным
        let components = Calendar.current.dateComponents([.minute], from: order_created_date, to: currentDate)

        if let created_minutes = components.minute {

            if created_minutes > 120 && order.order_status == .active {
                
                    cell.backgroundColor = redColor
                    cell.orderIdLabel.textColor = UIColor.white
                    cell.customerNameLabel.textColor = UIColor.white
                    cell.customerAddress.textColor = UIColor.white
                    cell.orderCreatedDate.textColor = UIColor.white
                
            } else {
                cell.backgroundColor = UIColor.white
                cell.orderIdLabel.textColor = UIColor.black
                cell.customerNameLabel.textColor = UIColor.black
                cell.customerAddress.textColor = UIColor.black
                cell.orderCreatedDate.textColor = UIColor.black
            }
        } else {
            cell.backgroundColor = UIColor.white
            cell.orderIdLabel.textColor = UIColor.black
            cell.customerNameLabel.textColor = UIColor.black
            cell.customerAddress.textColor = UIColor.black
            cell.orderCreatedDate.textColor = UIColor.black
        }

        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Orders.count
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.presentOrder(indexPath.row)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {

        let optionsAction = UIContextualAction(style: .normal, title:  "Опции", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.showOptions(index: indexPath.row)
            success(true)
        })
        optionsAction.backgroundColor = UIColor.lightGray
        
        return UISwipeActionsConfiguration(actions: [optionsAction])
    }
    
    func showOptions(index : Int){
        let alert = UIAlertController(title : "Опции", message : nil, preferredStyle :.actionSheet)
        
        alert.addAction(UIAlertAction(title : "Информация", style : .default, handler : {(UIAlertAction) -> Void in self.presentOrder(index)}))
        alert.addAction(UIAlertAction(title : "Позвонить", style : .default, handler : {(UIAlertAction) -> Void in self.callCustomer(phone: self.Orders[index].customer_phone)}))
        
        alert.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func presentOrder(_ index : Int){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Admin", bundle: nil)

        let chooseOrderController = storyBoard.instantiateViewController(withIdentifier: "AdminChooseOrderVC") as! AdminChooseOrderVC
        chooseOrderController.order_id = self.Orders[index].id
            
        self.navigationController?.pushViewController(chooseOrderController, animated: true)
    }
    

    func callCustomer(phone : String){
        if let url = URL(string: "tel://\(phone)"){
            UIApplication.shared.open(url)
        }
    }
}
