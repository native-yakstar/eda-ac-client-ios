//
//  ChooseOrderVC.swift
//  Eda.Ac
//
//  Created by dingo on 11.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON

struct Order {
    var code : String
    var id : String
    var customer_name : String
    var customer_phone : String
    var customer_address : String
    var status : Int
    var s_id : String
    var order_created_date : String
    var token : String
    var promocode_id : String
    var products : [adminFullProduct]
    var OrderTotalCount : Float
    var payType : PayType
    
    init() {
        code = String()
        id = String()
        customer_name = String()
        customer_phone = String()
        customer_address = String()
        status = 0
        s_id = String()
        order_created_date = String()
        token = String()
        promocode_id = String()
        products = [adminFullProduct]()
        OrderTotalCount = 0
        payType = .Cash
    }
}


class AdminChooseOrderVC : UITableViewController, UITextFieldDelegate {
    
    var ORDER = Order()
    
    @IBOutlet weak var orderId: UILabel!
    @IBOutlet weak var customerName: UITextField!
    @IBOutlet weak var customerPhone: UITextField!
    @IBOutlet weak var customerAddress: UITextField!
    @IBOutlet weak var orderStatus: UIButton!
    @IBOutlet weak var orderCreated: UILabel!
    @IBOutlet weak var orderPayType: UILabel!
    @IBOutlet weak var orderTotalCount: UILabel!
    
    var order_id = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customerName.delegate = self
        customerPhone.delegate = self
        customerAddress.delegate = self
        
        self.title = "Заказ"
        
        getOrder()
    }
    
    func getOrder(){
        var url = serverApiURL + "panel/orders"
        
        url += "/\(self.order_id)?access-token=\(adminAccessToken)"
        
        print(url)
        
        Alamofire.request(url).responseJSON { response in
            if let result = response.result.value {
                let json = JSON(result)
                
                
                self.ORDER.code = json["code"].stringValue
                self.ORDER.id = json["id"].stringValue
                self.ORDER.customer_name = json["customer_name"].stringValue
                self.ORDER.customer_phone = json["customer_phone"].stringValue
                self.ORDER.customer_address = json["customer_address"].stringValue
                self.ORDER.status = json["status"].intValue
                self.ORDER.s_id = json["s_id"].stringValue
                self.ORDER.order_created_date = json["datetime"].stringValue
                self.ORDER.token = json["token"].stringValue
                self.ORDER.promocode_id = json["promocode_id"].stringValue
                self.ORDER.payType = json["payment_type"] == "0" ? PayType.Cash : PayType.Card
                
                for product in json["products"].arrayValue {
                    var _product = adminFullProduct()
                    
                    _product.id = product["p_id"].stringValue
                    _product.title = product["title"].stringValue
                    _product.desc = product["desc"].stringValue
                    _product.image = product["image"].stringValue
                    _product.amount = product["amount"].intValue
                    _product.price = product["price"].floatValue
                    _product.s_id = product["s_id"].stringValue
                    _product.type = product["type"].stringValue == "1" ? .Promotion : .Product
                    
                    let productTotalCount = Float(_product.amount) * _product.price
                    
                    self.ORDER.OrderTotalCount += productTotalCount
                    
                    self.ORDER.products.append(_product)
                    
                }
                
                self.customerName.text = self.ORDER.customer_name
                self.customerAddress.text = self.ORDER.customer_address
                self.customerPhone.text = self.ORDER.customer_phone
                self.orderStatus.setTitle(ADMIN_ORDER_STATUSES[self.ORDER.status], for: .normal)
                
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: "RU_ru")
                
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                if let date = dateFormatter.date(from: self.ORDER.order_created_date) {
                    dateFormatter.dateFormat = "HH:mm,  dd MMM"
                    self.orderCreated.text = "Создан: \(dateFormatter.string(from: date))"
                    
                }
                
                self.orderId.text = "Номер заказа: №\(self.ORDER.id)"
                
                self.orderPayType.text = self.ORDER.payType == .Card ? "Безналичная оплата" : "Наличная оплата"
                self.orderTotalCount.text = "\(self.ORDER.OrderTotalCount) \(roubleSymbol)"
                
                self.tableView.reloadData()
                
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let storyBoard: UIStoryboard = UIStoryboard(name: "Admin", bundle: nil)
        
        let chooseProductController = storyBoard.instantiateViewController(withIdentifier: "AdminChooseProductVC") as! AdminChooseProductVC
        
        chooseProductController.PRODUCT = self.ORDER.products[indexPath.row]
        
        self.navigationController?.pushViewController(chooseProductController, animated: true)
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdminInOrderProductTableViewCell") as! AdminInOrderProductTableViewCell
        
        let product = ORDER.products[indexPath.row]
        
        if let url = URL(string: serverImageUrl + product.image){
            cell.productImage.af_setImage(withURL: url)
        }
        cell.productName.text = product.title
        
        cell.productCount.text = "Количество: \(product.amount) шт."
        cell.productPrice.text = "Стоимость: \(product.price) \(roubleSymbol)"
        cell.productTotalCount.text = "Итоговая стоимость: \(product.price * Float(product.amount)) \(roubleSymbol)"
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ORDER.products.count
    }
    
    @IBAction func changeOrderStatus(_ sender: Any) {
        let alert = UIAlertController(title : "Установите статус", message : nil, preferredStyle : .actionSheet)
        for index in 0 ..< ADMIN_ORDER_STATUSES.count {
            let orderStatus = ADMIN_ORDER_STATUSES[index]
            alert.addAction(UIAlertAction(title : orderStatus, style : .default, handler : {(UIAlertAction) -> Void in
                self.orderStatus.setTitle(orderStatus, for: .normal)
                self.ORDER.status = index
                
                self.showSaveButton()
            }))
        }
        alert.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func callButton(_ sender: Any) {
        if let url = URL(string: "tel://\(self.ORDER.customer_phone)"){
            UIApplication.shared.open(url)
        }
    }
    
    
    //Когда меняется uitextfield моментально меняется в объекте ORDER
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case customerName:
            ORDER.customer_name = textField.text!
            self.showSaveButton()
            break
        case customerPhone:
            ORDER.customer_phone = textField.text!
            self.showSaveButton()
            break
        case customerAddress:
            ORDER.customer_address = textField.text!
            self.showSaveButton()
            break
        default:
            break
        }
        
        
        return true
    }
    
    func showSaveButton(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Сохр.", style: .plain, target: self, action: #selector(saveChanges))
    }
    
    @objc func saveChanges(){

        
        
        
        //reformat basket
        
        var productsJSON = [JSON]()
        
        for product in self.ORDER.products {
            
            var jsonProduct = JSON()
            
            jsonProduct["p_id"].string = product.id
            jsonProduct["amount"].string = String(product.amount)
            jsonProduct["s_id"].string = product.s_id
            jsonProduct["type"].string = product.type == .Product ? "0" : "1"
            
            
            productsJSON.append(jsonProduct)
            
        }
        
        var ResponceJSON = JSON().dictionaryObject
        
        ResponceJSON = ["customer_name" : self.ORDER.customer_name,
                        "customer_address" : self.ORDER.customer_address,
                        "customer_phone" : self.ORDER.customer_phone,
                        "promocode_id" : self.ORDER.promocode_id,
                        "status" : self.ORDER.status,
                        "products" : productsJSON]
        
        
        
        //Upload
        let url = URL(string: serverApiURL + "panel/orders/update/\(self.ORDER.id)?access-token=\(adminAccessToken)")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = try! JSON(ResponceJSON!).rawData()
        
        Alamofire.request(request).responseJSON { response in
            
            if let result = response.result.value {
                let json = JSON(result)
                
                let alert = UIAlertController(title : json["msg"].stringValue, message : nil, preferredStyle : .alert)
                alert.addAction(UIAlertAction(title : "Закрыть", style : .cancel, handler : nil))
                
                self.present(alert, animated: true, completion: nil)
                
                print(json)
                
            }
        }
    }
}
