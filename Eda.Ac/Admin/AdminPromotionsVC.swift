//
//  PromotionsVC.swift
//  Eda.Ac
//
//  Created by dingo on 13.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit
import SwiftyJSON
import SwiftSpinner

class AdminPromotionsVC : UITableViewController {
    
    var Promotions = [adminFullPromotion]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Добавить", style: .plain, target: self, action: #selector(createPromotion))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getPromotions()

    }
    
    @objc func createPromotion(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Admin", bundle: nil)
        
        let choosePromotionController = storyBoard.instantiateViewController(withIdentifier: "AdminChoosePromotionVC") as! AdminChoosePromotionVC
        
        choosePromotionController.PROMOTION = adminFullPromotion()
        choosePromotionController.createPromotion = true
        
        self.navigationController?.pushViewController(choosePromotionController, animated: true)
    }
    
    func getPromotions(){
        SwiftSpinner.show("Загрузка")
        
        API().getProtions(completion: { promotions in
            self.Promotions = promotions
            self.tableView.reloadData()
            
            SwiftSpinner.hide()
        })
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdminPromotionCell") as! AdminPromotionCell
        
        let promotion = self.Promotions[indexPath.row]
        
        if let url = URL(string: serverImageUrl + promotion.image){
            cell.promotionImage.af_setImage(withURL: url)
        }
        
        cell.promotionTitle.text = promotion.title
        cell.promotionPrice.text = "Стоимость: \(promotion.price) \(roubleSymbol)"
        cell.promotionStatus.text = "Статус: \(promotion.status == "1" ? "Включен" : "Выключен")"
        cell.promotionProductCount.text = "Кол-во продуктов в акции: \(promotion.products.count) шт."
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Promotions.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Admin", bundle: nil)
        
        let choosePromotionController = storyBoard.instantiateViewController(withIdentifier: "AdminChoosePromotionVC") as! AdminChoosePromotionVC
        
        choosePromotionController.PROMOTION = self.Promotions[indexPath.row]
        
        self.navigationController?.pushViewController(choosePromotionController, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let optionsAction = UIContextualAction(style: .normal, title:  "Удалить", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.removePromocode(index: indexPath.row)
            success(true)
        })
        optionsAction.backgroundColor = UIColor.red
        
        return UISwipeActionsConfiguration(actions: [optionsAction])
    }
    
    @objc func removePromocode(index : Int) {
        let alert = UIAlertController(title : "Удалить акцию?", message : nil, preferredStyle : .alert)
        alert.addAction(UIAlertAction(title : "Удалить", style : .destructive, handler : { (UIAlertAction) -> Void in
            SwiftSpinner.show("Удаление")
            API().savePromotion(promotion: self.Promotions[index], handle: .remove, completion: { result in
                SwiftSpinner.hide()
                showAlert(result["msg"].stringValue, self)
                self.getPromotions()
            })
        }))
        alert.addAction(UIAlertAction(title : "Отмена", style : .cancel, handler : nil))
        self.present(alert, animated: true)
    }
    
}
