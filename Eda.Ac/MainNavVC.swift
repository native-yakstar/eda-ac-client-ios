//
//  MainNavVC.swift
//  Eda.Ac
//
//  Created by dingo on 09.12.2017.
//  Copyright © 2017 NATIVE. All rights reserved.
//

import UIKit

class MainNavVC : UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if (UserDefaults.standard.string(forKey: "legacy") == nil){
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let productController = storyBoard.instantiateViewController(withIdentifier: "LegacyVC") as! LegacyVC
            self.present(productController, animated: true, completion: nil)
        }else {
            if UserDefaults.standard.string(forKey: "admin-access-token") != nil {
                adminAccessToken = UserDefaults.standard.string(forKey: "admin-access-token")!
                
                let storyBoard: UIStoryboard = UIStoryboard(name: "Admin", bundle: nil)
                let productController = storyBoard.instantiateViewController(withIdentifier: "AdminNavController") as! AdminNavController
                self.present(productController, animated: true, completion: nil)
            }
        }
    }
    
}
